#!/bin/bash

DOCKER_PATH=$(dirname $(dirname $(readlink -f "$0")))
PROJECT_ROOT=$(dirname $DOCKER_PATH)
DJANGO_FOLDER=$PROJECT_ROOT

CPU_CORES=$(cat /proc/cpuinfo | grep processor | wc -l)
WORKERS=$(( 2 * CPU_CORES + 1))

python manage.py migrate
#python manage.py migrate --database=sisamex-legacy
#python manage.py migrate --database=cep

gunicorn --bind 0.0.0.0:8000 \
--name playground \
--workers $WORKERS \
--timeout 300 \
sisamex.wsgi:application