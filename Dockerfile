#Dockerfile
FROM python:3.6
RUN mkdir /application
WORKDIR "/application"
# Upgrade pip
# Update
RUN apt-get update \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

RUN apt-get -y update
RUN apt-get install -y vim
RUN apt-get -y update
RUN apt-get install -yqq unzip

RUN apt-get install -y python3-dev graphviz libgraphviz-dev pkg-config

RUN apt-get install -y unixodbc-dev unixodbc

RUN apt-get install -y freetds-dev freetds-bin unixodbc-dev tdsodbc

RUN apt-get -y update

ENV DISPLAY=:99

RUN echo '[FreeTDS]\nDescription=FreeTDS Driver\nDriver=/usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so\nSetup=/usr/lib/x86_64-linux-gnu/odbc/libtdsS.so\nUsageCount=1\nTrace=Yes' >> /etc/odbcinst.ini

RUN echo '[SQL Server]\nDriver=FreeTDS\nhostname=192.168.0.151\SQLEXPRESS\nDatabase=QDA_DAIMLER\nUID=QDA_Evient\nPWD=123456' >> /etc/odbc.ini

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update -y
RUN ACCEPT_EULA=Y apt-get install msodbcsql17 -y

ADD requirements.txt /application/
RUN pip install --default-timeout=2000 -r /application/requirements.txt
RUN pip install 'django-graphql-social-auth[jwt]'

COPY ./start.sh /start.sh
COPY ./entrypoint.sh /entrypoint.sh
RUN sed -i 's/\r//' /start.sh
RUN sed -i 's/\r//' /entrypoint.sh
RUN chmod +x /start.sh
RUN chmod +x /entrypoint.sh
