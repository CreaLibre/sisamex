from datetime import datetime, timedelta

from django.db.models import Q, TextField, Max, Min, Value
from django.db.models.functions import Concat
from django.apps import apps

from sisamex.celery import app

@app.task(name="scada.check_new_data_scada")
def check_new_data_scada():
    SisboxMaquinas = apps.get_model("scada", "SisboxMaquinas")
    Machine = apps.get_model("organization", "Machine")
    LocalBusiness = apps.get_model("organization", "LocalBusiness")
    machine_names = Machine.objects.all().values_list("name", flat=True)
    machines = SisboxMaquinas.objects.exclude(nombre_maquina__in=list(machine_names)).order_by("-pk")
    try:
        machines = machines[:50]
    except:
        pass
    for machine in machines:
        try:
            Machine.objects.create(
                name=machine.nombre_maquina,
                maintenance_plant=LocalBusiness.objects.all().order_by('-pk').first()
            )
        except:
            print("Machine already exists.")
    SisboxVariables = apps.get_model("scada", "SisboxVariables")
    Variable = apps.get_model("maintenance", "Variable")
    variable_names = Variable.objects.all().values_list("name", flat=True)
    variables = SisboxVariables.objects.exclude(nombre_variable__in=list(variable_names)).order_by("-pk")
    try:
        variables = variables[:100]
    except:
        pass
    for variable in variables:
        Variable.objects.create(
            name=variable.nombre_variable
        )
    SisboxValores = apps.get_model("scada", "SisboxValores")
    Measure = apps.get_model("maintenance", "Measure")
    values = SisboxValores.objects.filter(fecha__date=datetime.now().date()).order_by("-pk")
    try:
        values = values[:500]
    except:
        pass
    for value in values:
        try:
            Measure.objects.create(
                machine=Machine.objects.filter(name=value.id_maquina.nombre_maquina).order_by('-pk').first(),
                variable=Variable.objects.filter(name=value.id_variable.nombre_variable).order_by('-pk').first(),
                value=value.valor,
                full_date=value.fecha
            )
        except:
            print(value.fecha)


@app.task(name="scada.delete_old_measures")
def delete_old_measures():
    Measure = apps.get_model('maintenance', 'Measure')
    HistoricalMeasureAlerts = apps.get_model('maintenance', 'HistoricalMeasureAlerts')
    Variable = apps.get_model("maintenance", "Variable")
    Machine = apps.get_model("organization", "Machine")
    Measure.objects.exclude(created_at__date=datetime.now().date()).delete()
    alerts = HistoricalMeasureAlerts.objects.filter(created_at__date=(datetime.now() - timedelta(days=1)).date())
    unique_alerts = alerts.annotate(distinct_name=Concat('machine_id', 'variable_id', output_field=TextField()))\
        .order_by('distinct_name').distinct('distinct_name')
    for al in unique_alerts:
        threshold = al.variable.threshold_set.filter(machine=al.machine).first()
        if threshold is None:
            continue
        if threshold.threshold_type == threshold.LESS_THAN:
            min_val = HistoricalMeasureAlerts.objects.filter(machine=al.machine, variable=al.variable)\
                .annotate(common=Value(1))\
                .values('common')\
                .annotate(min_vals=Min('measure__value'))\
                .values('min_vals').order_by('min_vals').first()['min_vals']
            filt = HistoricalMeasureAlerts.objects.filter(
                machine=al.machine,
                variable=al.variable,
                measure__value=min_val).order_by('pk').first()
        elif threshold.threshold_type == threshold.GREATER_THAN:
            max_val = HistoricalMeasureAlerts.objects.filter(machine=al.machine, variable=al.variable)\
                .annotate(common=Value(1))\
                .values('common')\
                .annotate(max_vals=Max('measure__value'))\
                .values('max_vals').order_by('max_vals').first()['max_vals']
            filt = HistoricalMeasureAlerts.objects.filter(
                machine=al.machine,
                variable=al.variable,
                measure__value=max_val).order_by('pk').first()
        elif threshold.threshold_type == threshold.EQUALS:
            filt = HistoricalMeasureAlerts.objects.filter(
                machine=al.machine,
                variable=al.variable,
                measure__value=threshold.alert).order_by('pk').first()
        alerts.filter(machine=al.machine, variable=al.variable).exclude(pk=filt.pk).delete()
        
