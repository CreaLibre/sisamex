from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import SisboxMaquinas, SisboxValores, SisboxVariables

@admin.register(SisboxMaquinas)
class SisboxMaquinasAdmin(ImportExportModelAdmin):
    pass

@admin.register(SisboxValores)
class SisboxValoresAdmin(ImportExportModelAdmin):
    pass

@admin.register(SisboxVariables)
class SisboxVariablesAdmin(ImportExportModelAdmin):
    pass