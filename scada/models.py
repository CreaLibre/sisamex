# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils.translation import ugettext_lazy as _


class SisboxMaquinas(models.Model):

    _DATABASE = 'sisamex-legacy'

    id_maquina = models.AutoField(primary_key=True)
    nombre_maquina = models.CharField(max_length=10)

    def __str__(self):
        return "{} - {}".format(self.id_maquina, self.nombre_maquina)

    class Meta:
        verbose_name = _("Sisbox Machine")
        verbose_name_plural = _("Sisbox Machines")
        ordering = ['-pk']
        managed = False
        db_table = 'sisbox_maquinas'


class SisboxValores(models.Model):

    _DATABASE = 'sisamex-legacy'

    valor = models.FloatField()
    id_maquina = models.ForeignKey(SisboxMaquinas, models.DO_NOTHING, db_column='id_maquina')
    id_variable = models.ForeignKey('SisboxVariables', models.DO_NOTHING, db_column='id_variable')
    fecha = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return "{} - {}".format(self.id_maquina.nombre_maquina, self.id_variable.nombre_variable)

    class Meta:
        verbose_name = _("Sisbox Value")
        verbose_name_plural = _("Sisbox Values")
        ordering = ['-pk']
        managed = False
        db_table = 'sisbox_valores'


class SisboxVariables(models.Model):

    _DATABASE = 'sisamex-legacy'

    id_variable = models.AutoField(primary_key=True)
    nombre_variable = models.CharField(max_length=45)

    def __str__(self):
        return "{} - {}".format(self.id_variable, self.nombre_variable)

    class Meta:
        verbose_name = _("Sisbox Variable")
        verbose_name_plural = _("Sisbox Variables")
        ordering = ['-pk']
        managed = False
        db_table = 'sisbox_variables'
