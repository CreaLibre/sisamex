"""sisamex URL Configuration

The `urlpatterns` list sisamex URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from base.views import CustomGraphQLView, acme_challenge
from django.conf.urls.static import static
from graphene_django.views import GraphQLView
from graphql_jwt.decorators import jwt_cookie

urlpatterns = [
    path('admin/', admin.site.urls),
    path('graphql', jwt_cookie(csrf_exempt(GraphQLView.as_view(graphiql=True))), name='graphql'),
    path('.well-known/acme-challenge/p6X1iHm6FVcIjqJmU2gZow4AznUJmg90HFJCydVi7DQ', acme_challenge, name='acme')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
