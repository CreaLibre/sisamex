from __future__ import absolute_import, unicode_literals
import os
import sys
import django
from os import environ
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sisamex.settings')

app = Celery('sisamex')

app.config_from_object('django.conf.settings', namespace='CELERY')
django.setup()
from django.apps import apps 
app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()], force=True)

app.conf.beat_schedule = {
    'maintenance_alert_send_mail': {
        'task': 'maintenance.tasks.maintenance_alert_send_mail',
        'schedule': crontab(hour=12, minute=0, day_of_week='*'),
        'args': None
    },
    'cep_delete_old_entries': {
        'task': 'cep_legacy.delete_old_cep',
        'schedule': crontab(0, 0, day_of_month='*'),
        'args': None
    },
    'cep_new_data': {
        'task': 'cep.query_cep_transfer',
        'schedule': crontab(minute='*/15'),
        'args': None
    },
    'scada_check_new_data_scada': {
        'task': 'scada.check_new_data_scada',
        'schedule': crontab(minute='*'),
        'args': None
    },
    'maintenance_delete_old_measures': {
        'task': 'scada.delete_old_measures',
        'schedule': crontab(0, 0, day_of_month='*'),
        'args': None
    }
}
