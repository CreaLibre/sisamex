import graphene
import graphql_jwt
import graphql_social_auth

import maintenance.schema
import cep.schema
import users.schema
import organization.schema

class Query(
        maintenance.schema.Query,
        cep.schema.Query,
        users.schema.Query,
        organization.schema.Query,
        graphene.ObjectType):
    pass


class Mutations(
        maintenance.schema.Mutation,
        cep.schema.Mutation,
        organization.schema.Mutation,
        graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    social_auth = graphql_social_auth.SocialAuthJWT.Field()


schema = graphene.Schema(query=Query, mutation=Mutations)
