import itertools

from django.db.models import Prefetch

from graphql.utils.ast_to_dict import ast_to_dict

from django.db.models.fields.files import ImageField
from django.db.models import BooleanField
import pandas as pd



def _evaluate_node(node, to_visit, related_fields):
    name, deepth, data, model = node

    only_fields = []
    selected_fields = (data['selection_set']['selections']
                       if data['selection_set'] is not None else tuple())

    # Dictionary with selected fields
    selected_fields = dict((f['name']['value'], f) for f in selected_fields)
    # Filter only by selected fields
    fields = [f for f in model._meta.get_fields() if f.name in selected_fields]
    for field in fields:
        if field.one_to_many or field.many_to_one:
            related_fields['prefetch_related_fields'].append(
                name + '__' + field.name if name else field.name)

            to_visit.append((
                name + '__' + field.name if name else field.name,
                deepth + 1,
                selected_fields[field.name],
                field.related_model
            ))
        elif field.one_to_one:
            related_fields['selected_related_fields'].append(
                name + '__' + field.name if name else field.name)

            to_visit.append((
                name + '__' + field.name if name else field.name,
                deepth + 1,
                selected_fields[field.name],
                field.related_model
            ))
        else:
            only_fields.append(field.name)

    related_fields['only_fields'][name] = only_fields


def optimize_query(queryset, info):
    model = queryset.model
    root = ast_to_dict(info.field_asts[0])
    to_visit = []
    related_fields = {
        'only_fields': {},
        'selected_related_fields': [],
        'prefetch_related_fields': [],
    }

    # (parent_name, deepth, field_data, model)
    node = ('', 0, root, model)
    _evaluate_node(node, to_visit, related_fields=related_fields)

    while to_visit:
        node = to_visit.pop(0)
        _evaluate_node(node, to_visit, related_fields=related_fields)

    # Prefetch related
    for related in related_fields['prefetch_related_fields']:
        related_fields['only_fields'].pop(related)
        queryset = queryset.prefetch_related(related)

    # Select related
    for related in related_fields['selected_related_fields']:
        queryset = queryset.selected_related(related)

    # Only fields
    only_fields = itertools.chain(*related_fields['only_fields'].values())
    queryset = queryset.only(*only_fields)

    return queryset


def rename_csv_columns(df:pd.DataFrame, model):
    fields = model._meta.fields
    df = df.rename(columns=__new_columns_name(fields))
    return df


def __new_columns_name(fields):
    new_column_names = {}
    for column in fields:
        new_column_names[column.name] = column.verbose_name
    return new_column_names


# =============================================================

def get_str_in_df(df, column_name, model):
    def return_str_or_none(value):
        try:
            return model.objects.get(pk=value)
        except:
            return None
    return df[column_name].apply(return_str_or_none)


def get_true_or_false(df, column_name):
    return df[column_name].apply(lambda value: 'Verdadero' if int(value) else 'Falso')


def change_related_and_boolean_fields(df, model):
    fields = [field for field in model._meta.fields]
    related_fields = []
    boolean_fields = []

    for field in fields:
        try:
            related_fields.append(getattr(model, field.name).field)
        except:
            if type(field) == BooleanField:
                boolean_fields.append(field.name)
    related_fields = [field for field in related_fields if type(field) != ImageField]

    for field in related_fields:
        df[field.name] = get_str_in_df(df, field.name, field.related_model)
    for field in boolean_fields:
        df[field] = get_true_or_false(df, field)
    df = rename_csv_columns(df, model)
    return df
