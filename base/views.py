import inspect
import json
import ujson
import re
import six

from django.conf import settings
from django.http import HttpResponse, HttpResponseNotAllowed
from django.http.response import HttpResponseBadRequest
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.views.decorators.csrf import ensure_csrf_cookie

from graphql import get_default_backend
from graphql.error import format_error as format_graphql_error
from graphql.error import GraphQLError
from graphql.execution import ExecutionResult
from graphql.type.schema import GraphQLSchema

from graphene_django.settings import graphene_settings
from graphene_django.views import GraphQLView, HttpError


from datetime import timedelta
settings_auth = {
    'TOKEN_TTL': timedelta(hours=10),
    'AUTO_REFRESH': False,
    'MIN_REFRESH_INTERVAL': 60,
    'AUTH_USE_SECURE_COOKIES': False,
}


class CustomGraphQLView(GraphQLView):

    def _get_json_parser():
        # if graphene_settings.PARSER.lower() == 'ujson':
        if settings.DEBUG:
            return json
        else:
            return ujson

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, request, *args, **kwargs):
        json_parser = CustomGraphQLView._get_json_parser()

        try:
            if request.method.lower() not in ("get", "post"):
                raise HttpError(
                    HttpResponseNotAllowed(
                        ["GET", "POST"],
                        "GraphQL only supports GET and POST requests."
                    )
                )

            data = self.parse_body(request)
            show_graphiql = self.graphiql and self.can_display_graphiql(
                request, data)

            if self.batch:
                responses = [
                    self.get_response(request, entry) for entry in data]
                result = "[{}]".format(
                    ",".join([response[0] for response in responses])
                )
                status_code = (
                    responses
                    and max(responses, key=lambda response: response[1])[1]
                    or 200
                )
            else:
                result, status_code = self.get_response(
                    request, data, show_graphiql)

            if show_graphiql:
                query, variables, operation_name, id = self.get_graphql_params(
                    request, data
                )
                return self.render_graphiql(
                    request,
                    graphiql_version=self.graphiql_version,
                    query=query or "",
                    variables=json_parser.dumps(variables) or "",
                    operation_name=operation_name or "",
                    result=result or "",
                )

            response = HttpResponse(
                status=status_code, content=result,
                content_type="application/json"
            )

            # Authentication Token is added to Cookies on the response
            if hasattr(request, '_token'):
                response.set_cookie(
                    'Token', request._token, httponly=True, max_age=315360000,
                    secure=settings_auth['AUTH_USE_SECURE_COOKIES']
                )

            return response

        except HttpError as e:
            response = e.response
            response["Content-Type"] = "application/json"
            response.content = self.json_encode(
                request, {"errors": [self.format_error(e)]}
            )
            return response


    def json_encode(self, request, d, pretty=False):
        json_parser = CustomGraphQLView._get_json_parser()

        if not (self.pretty or pretty) and not request.GET.get("pretty"):
            if json_parser is ujson:
                return json_parser.dumps(d, ensure_ascii=True)
            else:
                return json_parser.dumps(d, separators=(",", ":"))

        if json_parser is ujson:
            return json_parser.dumps(d, ensure_ascii=True)
        else:
            return json_parser.dumps(
                d, sort_keys=True, indent=2, separators=(",", ": "))

    def parse_body(self, request):
        json_parser = CustomGraphQLView._get_json_parser()
        content_type = self.get_content_type(request)

        if content_type == "application/graphql":
            return {"query": request.body.decode()}

        elif content_type == "application/json":
            # noinspection PyBroadException
            try:
                body = request.body.decode("utf-8")
            except Exception as e:
                raise HttpError(HttpResponseBadRequest(str(e)))

            try:
                request_json = json_parser.loads(body)
                if self.batch:
                    assert isinstance(request_json, list), (
                        "Batch requests should receive a list, but received {}."
                    ).format(repr(request_json))
                    assert (
                        len(request_json) > 0
                    ), "Received an empty list in the batch request."
                else:
                    assert isinstance(
                        request_json, dict
                    ), "The received data is not a valid JSON query."
                return request_json
            except AssertionError as e:
                raise HttpError(HttpResponseBadRequest(str(e)))
            except (TypeError, ValueError):
                raise HttpError(HttpResponseBadRequest("POST body sent invalid JSON."))

        elif content_type in [
            "application/x-www-form-urlencoded",
            "multipart/form-data",
        ]:
            return request.POST

        return {}

    @staticmethod
    def get_graphql_params(request, data):
        json_parser = CustomGraphQLView._get_json_parser()

        query = request.GET.get("query") or data.get("query")
        variables = request.GET.get("variables") or data.get("variables")
        id = request.GET.get("id") or data.get("id")

        if variables and isinstance(variables, six.text_type):
            try:
                variables = json_parser.loads(variables)
            except Exception:
                raise HttpError(
                    HttpResponseBadRequest("Variables are invalid JSON."))

        operation_name = request.GET.get(
            "operationName") or data.get("operationName")
        if operation_name == "null":
            operation_name = None

        return query, variables, operation_name, id


def acme_challenge(request, *args, **kwargs):
    with open(file='/application/p6X1iHm6FVcIjqJmU2gZow4AznUJmg90HFJCydVi7DQ') as f:
        response = f.read()
    return HttpResponse(response)
