import binascii
import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

import graphene
from graphene import Field, InputField
from graphene.types.utils import yank_fields_from_attrs
from graphene.types.mutation import MutationOptions

from graphene_django.forms.types import ErrorType
from graphene_django.forms.mutation import (
    DjangoFormMutation, DjangoModelFormMutation)

from graphql_relay.node.node import from_global_id
from graphene.relay.mutation import ClientIDMutation


class CreateMutation(DjangoFormMutation):
    """
        Usage example:

        class CreateObject(CreateMutation):
            errors = graphene.List(ErrorType)
            object = graphene.Field(objecttNode)

            class Meta:
                form_class = ObjectForm

            @classmethod
            def perform_create(cls, create_cls, form, info):
                object = form.save()
                return cls(errors=[], object=object)
    """
    success = graphene.Boolean(default_value=True)

    class Meta:
        abstract = True

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        form = cls.get_form(root, info, **input)
        
        if form.is_valid():
            return cls.perform_mutate(form, info)
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]
            return cls(success=False, errors=errors)

    @classmethod
    def perform_mutate(cls, form, info):
        if hasattr(cls, 'perform_create'):
            return cls.perform_create(cls, form, info)
        else:
            return super().perform_mutate(form, info)

    @classmethod
    def get_form(cls, root, info, **input):
        form_kwargs = cls.get_form_kwargs(root, info, **input)
        form_kwargs['files'] = info.context.FILES
        return cls._meta.form_class(**form_kwargs)


class UpdateMutation(DjangoModelFormMutation):
    """
        Usage example:

        class UpdateObject(CreateMutation):
            errors = graphene.List(ErrorType)
            object = graphene.Field(objecttNode)

            class Meta:
                model = Model
                form_class = ObjectForm

            @classmethod
            def perform_update(cls, update_cls, form, info):
                object = form.save()
                return cls(errors=[], object=object)
    """
    success = graphene.Boolean(default_value=True)

    class Meta:
        abstract = True

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        form = cls.get_form(root, info, **input)
        if form.is_valid():
            return cls.perform_mutate(form, info)
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(success=False, errors=errors)

    @classmethod
    def perform_mutate(cls, form, info):
        if hasattr(cls, 'perform_update'):
            return cls.perform_update(cls, form, info)
        else:
            return super().perform_mutate(form, info)

    @classmethod
    def get_form(cls, root, info, **input):
        form_kwargs = cls.get_form_kwargs(root, info, **input)
        form_kwargs['files'] = info.context.FILES

        # Django forms by default don't support partial update
        # This is a custom class to update only sent fields on an instance
        on_update_fields = form_kwargs["data"].keys()
        class PartialUpdateFormClass(cls._meta.form_class):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

                # Only update fields sent as input to mutation
                for field in tuple(self.fields.keys()):
                    if field in on_update_fields:
                        self.fields[field].required = False
                    else:
                        self.fields.pop(field)

        form = PartialUpdateFormClass(**form_kwargs)
        return form

    @classmethod
    def get_form_kwargs(cls, root, info, **input):
        
        kwargs = {"data": input}
        try:
            pk = int(from_global_id(input.pop("id", ''))[1])
            instance = cls._meta.model._default_manager.get(pk=pk)
            kwargs["instance"] = instance
        except TypeError:
            raise ValidationError(_('Invalid Global ID'))
        except binascii.Error as error:
            raise ValidationError(_('Invalid Global ID'))
        except cls._meta.model.DoesNotExist as error:
            raise ValidationError(_('Invalid Global ID'))

        return kwargs


class DeleteMutationOptions(MutationOptions):
    model = None


class DeleteMutation(ClientIDMutation):
    """
        Usage example:

        class DeleteObject(CreateMutation):
            errors = graphene.List(ErrorType)
            object = graphene.Field(objecttNode)

            class Meta:
                form_class = ObjectForm

            @classmethod
            def perform_delete(cls, delete_cls, instance, info):
                object = instance.delete()
                return cls(errors=[], object=instance)
    """
    success = graphene.Boolean(default_value=True)

    class Meta:
        abstract = True

    class Input:
        id = graphene.ID()

    errors = graphene.List(ErrorType)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        form = cls.get_form(root, info, **input)

        if form.is_valid():
            return cls.perform_mutate(form, info)
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(success=False, errors=errors)

    @classmethod
    def perform_mutate(cls, instance, info):
        if hasattr(cls, 'perform_delete'):
            return cls.perform_delete(cls, instance, info)
        else:
            instance.delete()
            return cls(errors=[])

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        instance = None

        try:
            pk = int(from_global_id(input.pop("id", ''))[1])
            instance = cls._meta.model._default_manager.get(pk=pk)
        except ValueError as error:
            raise Exception(_('Invalid Global ID'))
        except TypeError:
            raise Exception(_('Invalid Global ID'))
        except binascii.Error as error:
            raise Exception(_('Invalid Global ID'))
        except cls._meta.model.DoesNotExist as error:
            raise Exception(_('Node does not exists'))

        return cls.perform_mutate(instance, info)

    @classmethod
    def __init_subclass_with_meta__(cls, model=None, **options):

        _meta = DeleteMutationOptions(cls)
        _meta.model = model

        super(DeleteMutation, cls).__init_subclass_with_meta__(
            _meta=_meta, **options
        )


class GenericMutations(graphene.ObjectType):

    @classmethod
    def __init_subclass_with_meta__(
            cls, generic_name='generic', model=None, form_class=None,
            methods=['create', 'update', 'delete'], **options
            ):

        if cls.__bases__[0] is GenericMutations:
            methods = [item.lower() for item in methods]

            # Normalize generic name, if has '_' removes it
            if len(generic_name) > 1 and '_' not in generic_name:
                generic_name = generic_name[0].upper() + generic_name[1:]
            elif len(generic_name) > 1 and '_' in generic_name:
                generic_name = ''.join(
                    [chunk.title() for chunk in generic_name.split('_')])

            subclass_fields = list(
                set(dir(cls)) - set(dir(graphene.ObjectType)) - set(['_meta'])
            )

            attrs = {}
            for field in subclass_fields:
                attrs[field] = getattr(cls, field, None)

            # Create Mutation
            if 'create' in methods:
                create_attrs = attrs.copy()
                create_attrs.update({'Meta': {'form_class': form_class}})
                create_name = 'create' + generic_name + 'Mutation'

                CustomCreateMutation = type(
                    create_name, (CreateMutation,), create_attrs)
                setattr(cls, create_name, CustomCreateMutation.Field())

            # Update Mutation
            if 'update' in methods:

                class PartialUpdateFormClass(form_class):
                    def __init__(self, *args, **kwargs):
                        super().__init__(*args, **kwargs)
                        for field in self.fields:
                            self.fields[field].required = False

                update_attrs = attrs.copy()
                update_attrs.update(
                    {'Meta': {
                        'form_class': PartialUpdateFormClass,
                        'model': model }}
                )
                update_name = 'update' + generic_name  + 'Mutation'

                CustomUpdateMutation = type(
                    update_name, (UpdateMutation,), update_attrs)
                setattr(cls, update_name, CustomUpdateMutation.Field())

            # Delete Mutation
            if 'delete' in methods:
                delete_attrs = attrs.copy()
                delete_attrs.update({'Meta': {'model': model}})
                delete_name = 'delete' + generic_name  + 'Mutation'

                CustomDeleteMutation = type(
                    delete_name, (DeleteMutation,), delete_attrs)
                setattr(cls, delete_name, CustomDeleteMutation.Field())

            # Delete unecesary fields
            for field in subclass_fields:
                delattr(cls, field)

        super(GenericMutations, cls).__init_subclass_with_meta__(
            **options
        )
