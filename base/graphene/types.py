import six
from graphql.language.ast import (
    BooleanValue, FloatValue, IntValue, StringValue)

from graphene.types import * # JSONString, String


class JSONType(JSONString):
    @staticmethod
    def serialize(dt):
        return dt
