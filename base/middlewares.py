from django.contrib.auth import authenticate
from django.contrib.auth.middleware import get_user
from django.contrib.auth.models import AnonymousUser
from django.http import JsonResponse
from django.utils.cache import patch_vary_headers
from django.utils.deprecation import MiddlewareMixin

from auths.auth import TokenAuthentication


class TokensAuthMiddleware(MiddlewareMixin):

    def resolve(self, next, root, info, **kwargs):
        context = info.context

        # If user has not been authenticated by other means and
        # query is not a query of introspection
        # info.operation.name.value.lower() != 'introspectionquery'
        is_introspection = False
        if info.operation.name and \
                hasattr(info.operation.name, 'value') and \
                info.operation.name.value.lower() == 'introspectionquery':
            is_introspection = True

        if context.user is not None and not is_introspection:
            token_authentication = TokenAuthentication()
            auth = token_authentication.authenticate(context)
            if auth:
                context.user = auth['user']
                context.token = auth['token']

        return next(root, info, **kwargs)
