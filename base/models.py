import logging

import uuid
from collections import Iterable

from django.db import models
from django.utils.translation import ugettext_lazy as _


logger = logging.getLogger(__name__)


class Model(models.Model):
    uuid = models.UUIDField(_('uuid'), unique=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._loaded_values = None
        self.__class__.add_verbose_name()

    @classmethod
    def add_verbose_name(cls, *args, **kwargs):
        logger.debug('Initial process.')
        
        # cls.attrs = cls._meta.get_fields()
        # logger.debug('For starts.')
        # for attr in cls.attrs:
        #     try:
        #         attr.field.verbose_name = _(attr.field.name)
        #     except TypeError:
        #         attr.field.related_name = _(attr.field.name)
        #     except:
        #         pass
        

    @classmethod
    def from_db(cls, db, field_names, values):
        instance = super().from_db(db, field_names, values)
        # customization to store the original field values on the instance
        instance._loaded_values = dict(zip(field_names, values))
        return instance

    def exists(self, *args):
        for field in args:
            if not hasattr(self, field):
                return False

        return True