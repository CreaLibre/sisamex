-- Table: public.hommel_registro

-- DROP TABLE public.hommel_registro;

CREATE TABLE public.hommel_registro
(
    id integer NOT NULL DEFAULT nextval('hommel_registro_id_seq'::regclass),
    date_and_time timestamp with time zone,
    operator character varying(20) COLLATE pg_catalog."default",
    serial_number character varying(100) COLLATE pg_catalog."default",
    colada character varying(20) COLLATE pg_catalog."default",
    measure_type character varying(50) COLLATE pg_catalog."default",
    machine_id integer,
    file_id integer,
    CONSTRAINT hommel_registro_pkey PRIMARY KEY (id),
    CONSTRAINT hommel_registro_file_id_e7dcfbd3_fk_hommel_archivo_id FOREIGN KEY (file_id)
        REFERENCES public.hommel_archivo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT hommel_registro_machine_id_68930079_fk_organization_machine_id FOREIGN KEY (machine_id)
        REFERENCES public.organization_machine (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.hommel_registro
    OWNER to evient;
-- Index: hommel_registro_file_id_e7dcfbd3

-- DROP INDEX public.hommel_registro_file_id_e7dcfbd3;

CREATE INDEX hommel_registro_file_id_e7dcfbd3
    ON public.hommel_registro USING btree
    (file_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: hommel_registro_machine_id_68930079

-- DROP INDEX public.hommel_registro_machine_id_68930079;

CREATE INDEX hommel_registro_machine_id_68930079
    ON public.hommel_registro USING btree
    (machine_id ASC NULLS LAST)
    TABLESPACE pg_default;