import * as React from 'react'
import * as ReactDOM from 'react-dom'

import MultiForm from './sisamex/'

ReactDOM.render(
    <MultiForm />,
    document.getElementById('root')
)