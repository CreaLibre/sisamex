
export const getValue = (data: object, variables: string) => {
    try {
        const vars = variables.split('.')
        let auxData = data
        vars.forEach(v => {
            auxData = auxData[v]
        })
    } catch {
        return false
    }
}

