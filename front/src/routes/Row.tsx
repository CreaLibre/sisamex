import * as React from 'react'

import { getValue } from '../common/'

interface RowProps {
    data: object,
    inputHandler: object
}

const Row = (props: RowProps) => {
    return (
        <div>
            <input type="text" name="to" value={getValue(props.data, '')} />
            <input type="text" name="from" value={getValue(props.data, '')} />
        </div>
    )
}

export default Row