const path = require("path")
const webpack = require("webpack")

module.exports = {
  entry: {
    vendor: ["react", "react-dom"],
    main: [
      "react-hot-loader/patch",
      "babel-runtime/regenerator",
      "webpack-hot-middleware/client?reload=true",
      "./src/index.tsx"
    ]
  },
  mode: "development",
  output: {
    filename: "js/[name]-bundle.js",
    path: path.resolve(__dirname, "../../sisamex/static/dist/"),
    publicPath: "/"
  },
  devServer: {
    contentBase: "dist",
    overlay: true,
    stats: {
      colors: true
    }
  },
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader"
          }
        ]
      },
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
            {
                loader: "ts-loader"
            }
        ]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader",
            options: {
              outputPath: 'css/'
            }
          },
          { loader: "css-loader" }
        ]
      },
      {
        test: /\.s[c|a]ss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[hash].[ext]',
              outputPath: 'images/'
            }
          }
        ]
      },
    //   {
    //     test: /\.html$/,
    //     use: [
    //       {
    //         loader: "file-loader",
    //         options: {
    //           name: "[name].[ext]"
    //         }
    //       },
    //       { loader: "extract-loader" },
    //       {
    //         loader: "html-loader",
    //         options: {
    //           attrs: ["img:src"]
    //         }
    //       }
    //     ]
    //   },
    //   {
    //     test: /\.md$/,
    //     use: [
    //       {
    //         loader: "markdown-with-front-matter-loader"
    //       }
    //     ]
    //   }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("development"),
        WEBPACK: true
      }
    })
    ,
    new webpack.HotModuleReplacementPlugin()
  ]
}
