IF DB_ID('QDA_DAIMLER') IS NOT NULL
BEGIN
    USE QDA_DAIMLER;
    IF NOT EXISTS (SELECT * 
    FROM INFORMATION_SCHEMA.TABLES 
    WHERE TABLE_SCHEMA = 'DBO' 
        AND  TABLE_NAME = 'DAT_MESSUNG')
    BEGIN
        USE master;
        RESTORE DATABASE [QDA_DAIMLER]
            FROM DISK = N'QDA_DAIMLER.bak' WITH FILE = 1, 
            MOVE N'QDA_DAIMLER' TO N'/var/opt/mssql/data/QDA_DAIMLER.mdf', 
            MOVE N'QDA_DAIMLER_log' TO N'/var/opt/mssql/data/QDA_DAIMLER_log.ldf',
            NOUNLOAD,
            REPLACE,
            STATS = 5;
    END
END
