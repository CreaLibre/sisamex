IF DB_ID('QDA_DAIMLER') IS NOT NULL
BEGIN
    USE QDA_DAIMLER;
    IF NOT EXISTS (SELECT [name]
                FROM [sys].[database_principals]
                WHERE [type] = N'S' AND [name] = N'QDA_Evient')
    BEGIN
        CREATE LOGIN QDA_Evient WITH PASSWORD = 'UF5dBArD6TyQHYmNa';
        CREATE USER QDA_Evient FOR LOGIN QDA_Evient;
        EXEC sp_addrolemember 'db_owner', 'QDA_Evient';
    END
END
