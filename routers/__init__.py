
class DefaultLegacyRouter(object):

    def db_for_read(self, model, **hints):
        return getattr(model, "_DATABASE", "sisamex-legacy")

    def db_for_write(self, model, **hints):
        return getattr(model,  "_DATABASE", "sisamex-legacy")