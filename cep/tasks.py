import requests, pyodbc, os
from requests.auth import HTTPBasicAuth
from datetime import datetime
from django.db import connections

from celery.decorators import task
from celery.utils.log import get_task_logger
from sisamex.celery import app

from django.apps import apps

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

@app.task
def query_data_transfer():
    #FIXME: DRY in line 46
    server = f"{os.environ.get('DAIMLER_DB_HOST')},{os.environ.get('DAIMLER_DB_PORT')}"
    database = os.environ.get('DAIMLER_DB_NAME')
    username =  os.environ.get('DAIMLER_DB_USER')
    password = os.environ.get('DAIMLER_DB_PASSWORD')
    daimler_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    result = None
    with daimler_conn.cursor() as cursor:
        cursor.execute('''
            SELECT TOP 15 VIEW_MESSUNGDATENAUFTRAG.PRFNR AS ID, VIEW_MESSUNGDATENAUFTRAG.DATUM AS DATE, TEIL AS PARTE, VORGANGBEZ AS OPERACION, DAT_MESSUNG.SUCH4 AS MAQUINA , DAT_MESSUNG.SUCH5 AS TIPODEMEDICION, DAT_MESSUNG.SUCH3 AS DMC, MERKMALBEZ AS CARACTERISTICA , GEWM AS CRITICIDAD, X1 AS VALORACTUAL, NOMINAL AS NOMINAL, OT AS USL, UT AS LSL, OEGX AS UCL, UEGX AS LCL, X30 AS JUICIO
            FROM VIEW_MESSUNGDATENAUFTRAG
            INNER JOIN DAT_MESSUNG ON DAT_MESSUNG.PRFNR = VIEW_MESSUNGDATENAUFTRAG.PRFNR
            WHERE X1 IS NOT NULL  AND GEWM >= 3  AND SUCH5   LIKE 'Produccion'
            ORDER BY VIEW_MESSUNGDATENAUFTRAG.PRFNR DESC
        ''')
        result = dictfetchall(cursor)
    if result is not None:
        #TODO: save result in new model based on query
        #Model.objects.bulk_create([Model(**q) for q in result])
        ###This will save on db the result as it is, after that the db will make the machine relation through a trigger
        pass

@app.task(name="cep.query_cep_transfer")
def query_cep_transfer():
    server = f"{os.environ.get('DAIMLER_DB_HOST')},{os.environ.get('DAIMLER_DB_PORT')}"
    database = os.environ.get('DAIMLER_DB_NAME')
    username =  os.environ.get('DAIMLER_DB_USER')
    password = os.environ.get('DAIMLER_DB_PASSWORD')
    daimler_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    result = None
    with daimler_conn.cursor() as cursor:
        cursor.execute('''
            SELECT STDGESAMT.ERSTELLDATUM AS FECHA,TEIL as Parte, PRUEFPLAN AS PlanInspeccion , VORGANGBEZ AS Maquina, MERKMALBEZ AS IDCharacteristrics, NOMINAL, OT AS LSC,UT AS LIC,MINIMUM AS
            LIE, MAXIMUM AS LSE, XQ AS PROMEDIO,SIGMA AS StDev,CP,CPK,CPKU,CPKL
            FROM STDGESAMTDETAIL
            INNER JOIN STDGESAMT ON STDGESAMTDETAIL.NR = STDGESAMT.NR
            WHERE STDGESAMT.BEZEICHNUNG = 'Analytics'
            AND GEWM = 4
        ''')
        result = dictfetchall(cursor)
    if result is not None:
        Operation = apps.get_model('cep', 'Operation')
        Machine = apps.get_model('organization', 'Machine')
        File = apps.get_model('cep', 'File')
        Map = apps.get_model('cep', 'Map')
        MapFields = apps.get_model('cep', 'MapFields')
        HommelRegistryFields = apps.get_model('cep', 'HommelRegistryFields')
        HommelRegistry = apps.get_model('cep', 'HommelRegistry')
        LocalBusiness = apps.get_model('organization', 'LocalBusiness')
        #SpecificationLimit = apps.get_model('cep', 'SpecificationLimit')
        for res in result:
            # op = Operation.objects.filter(name=res['Operacion']).first()
            op = Operation.objects.filter(name='OP150').first()
            if op is None:
                # op = Operation.objects.create(name=res['Operacion'])
                op = Operation.objects.create(name='OP150')
            mach = Machine.objects.filter(name=res['Maquina'].replace('_', '-')).first()
            if mach is None:
                mach_name = res['Maquina']
                if mach_name[:2] == "OP":
                    continue
                mach = Machine.objects.create(
                    maintenance_plant=LocalBusiness.objects.all().first(),
                    name=res['Maquina'].replace('_', '-')
                )
            _file = File.objects.filter(name='Unknown', number_part=res['Parte']).first()
            if _file is None:
                _file = File.objects.create(
                    name='Unknown',
                    prefix='???',
                    number_part=res['Parte'],
                    operation=op
                )
            _map = Map.objects.filter(file=_file).first()
            if _map is None:
                _map = Map.objects.create(file=_file)
            map_field = MapFields.objects.filter(field_name=res['IDCharacteristrics']).first()
            if map_field is None:
                map_field = MapFields.objects.create(
                    map=_map,
                    field_name=res['IDCharacteristrics'],
                )
            # _sl = SpecificationLimit.objects.filter(map_field=map_field).first()
            # if _sl is None:
            #     _sl = SpecificationLimit.objects.create(
            #         map_field=map_field,
            #         upper_limit=res['LSE'],
            #         lower_limit=res['LIE'],
            #         nominal=res['NOMINAL'],
            #         file=_file
            #     )
            _hr = HommelRegistry.objects.create(
                file=_file,
                date_and_time=res['FECHA'],
                machine=mach
            )
            HommelRegistryFields.objects.create(
                registry=_hr,
                map_fields=map_field,
                value_field=res['NOMINAL']
            )
            

@app.task(name="cep.mtconnect_data_transfer")
def mtconnect_data_transfer():
    Machine = apps.get_model('organization', 'Machine')
    MtConnect = apps.get_model('cep', 'MtConnect')
    variables = []
    result = requests.get("http://172.16.100.99:38000/api/v1/resumenETE/", auth=HTTPBasicAuth('fomix_app_movil', 'Sisamex.2020')).json()['results']
    for r in result:
        try:
            machine = Machine.objects.get(name=r['nombre_maquina'])
        except:
            machine = None
        f1 = r['fecha_creacion'].split('T')
        f2 = f1[1].split('.')[0]
        date_str = '{} {}'.format(f1[0], f2)
        variables.append(MtConnect(
            machine=machine,
            name=r['variables'],
            value=r['valor'],
            creation_date=datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
        ))
    MtConnect.objects.bulk_create(variables)