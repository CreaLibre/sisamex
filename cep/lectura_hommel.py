#!/usr/bin/env python
#-*- coding: utf-8 -*-
import sys
import csv
import os
import logging
import logging.handlers
import django
from datetime import datetime
from os.path import isfile
from os.path import join
from os import listdir
from smb.SMBConnection import SMBConnection
from smb.SMBHandler import SMBHandler
import urllib




sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sisamex.settings")
django.setup()

from cep.models import (
    File,
    Map,
    MapFields,
    HommelRegistry,
    HommelRegistryFields
)

def obt_archivo(logger):
    #Se obtienen los archivos por medio del protocolo smb
    folder = os.environ.get('FOLDER')
    userID = os.environ.get('USER_SMB')
    password = os.environ.get('PASSWORD_SMB')
    client_machine_name = os.environ.get('CLIENTE_MACHINE')
    server_name = os.environ.get('SERVER_NAME')
    server_ip = os.environ.get('SERVER_IP')
    domain_name = os.environ.get('DOMAIN_NAME')

    conn = SMBConnection(userID, password, client_machine_name, server_name, domain=domain_name, use_ntlm_v2=True,
                     is_direct_tcp=True)
    conn.connect(server_ip, 445)
    lst_archivos = conn.listPath(folder,'/')
    for arch in lst_archivos:
        if arch.file_size > 0:
            m_url = "smb://%s@%s:%s@%s/%s/%s" % (
                userID,
                domain_name,
                password,
                server_ip,
                folder,
                arch.filename
            )
            director = urllib2.build_opener(SMBHandler)
            smb_arch = director.open(m_url)
            analiza_archivo(smb_arch, arch.filename, logger)
            smb_arch.close()
    conn.close()


def analiza_archivo(arch, archivo, logger):
    logger.info("Analizando archivo: [%s]" % archivo)
    renglones = csv.reader(arch)
    con = 0
    for ren in renglones:
        if con == 0:
            titulos = ren
        else:
            #print ren
            dat_reg = {
                'archivo' : archivo,
                'fecha' : ren[0],
                'hora' : ren[1],
                'operador': ren[2],
                'numero_serie' : ren[3],
                'colada': ren[4],
                'tipo_medicion': ren[5],
                'maquina': ren[6],
            }
            #print dat_reg
            er = existe_ren(dat_reg, logger)
            if er == 2:
                logger.info("Nuevo registro: %s" % dat_reg)
                oReg = guardar_registro(dat_reg)
                guardar_campos(oReg, ren, titulos, logger)
            elif er == 1:
                #print "si existe: %s" % dat_reg
                logger.warning("el registro YA existe: %s" % dat_reg)
            elif er == 0:
                return

        con = con + 1
    logger.info("renglones analizados: %s" % con)
    logger.info("-------------------------------")


def existe_ren(dat_reg, logger):
    #0(Archivo no esta configurad), 1(Registro ya existe), 2(Nuevo registro)
    r = 0
    try:
        oArch = File.objects.get(name=dat_reg.get('archivo'))
    except:
        oArch = None
        #print "No esta definido el archivo"
        logger.warning("el archivo [%s] no esta configurado, no sera analizado" % dat_reg.get('archivo'))

    if oArch:
        try:
            oReg = HommelRegistry.objects.get(
                file=oArch,
                date_and_time=datetime.strptime("{} {}".format(dat_reg.get('fecha'), dat_reg.get('hora')),'%Y-%m-%d %H:%M:%S'),
                serial_number=dat_reg.get('numero_serie'),
            )
            r = 1
        except:
            r = 2
            pass

    return r

def guardar_registro(dat_reg):
    oArch = File.objects.get(name=dat_reg.get('archivo'))
    oReg = HommelRegistry.objects.create(
        archivo=oArch,
        fecha=datetime.strptime(dat_reg.get('fecha'),'%Y-%m-%d'),
        hora=datetime.strptime(dat_reg.get('hora'),'%H:%M:%S'),
        operador=dat_reg.get('operador'),
        numero_serie=dat_reg.get('numero_serie'),
        colada=dat_reg.get('colada'),
        tipo_medicion=dat_reg.get('tipo_medicion'),
        maquina=dat_reg.get('maquina'),
    )
    return oReg

def guardar_campos(oReg, ren, titulos, logger):
    try:
        oMap = Map.objects.get(archivo=oReg.archivo)
    except:
        #print "No tiene mapa definido"
        logger.warning("el rearchivo [%s] no tiene mapa, no sera analizado" % oReg.archivo)
        return

    oMapCamp = MapFields.objects.filter(
        mapa=oMap
    )
    #print "ren: "
    #print ren

    if oMapCamp:
        lst_dat=[]
        grabar_campo = True
        for campo in oMapCamp:
            if campo.validar:
                if campo.campo_nombre.strip() == titulos[campo.columna-1].strip():
                    grabar_campo = True
                else:
                    grabar_campo = False
                    #print "El campo no concide con los titulos del archivo"
                    logger.warning("El titulo [%s] no concide con el nombre del campo [%s] configurado" % (titulos[campo.columna-1].strip(),campo.campo_nombre.strip()))
            if grabar_campo:
                m_campo_valor = ren[campo.columna-1]
                if len(m_campo_valor) == 0:
                    m_campo_valor = 0

                inst = HommelRegistryFields(
                    registro=oReg,
                    campo_nombre=campo,
                    campo_valor=m_campo_valor
                )

                lst_dat.append(inst)
                grabar_campo = True

        if lst_dat:
            HommelRegistryFields.objects.bulk_create(lst_dat)


if __name__ == "__main__":
    #configura archivo log
    logger=logging.getLogger('lectura_hommel')
    logger.setLevel(logging.DEBUG)
    handler = logging.handlers.RotatingFileHandler(filename='logs/lectura_hommel.log', mode='a', maxBytes=5120000, backupCount=5)
    formatter = logging.Formatter(fmt='%(asctime)s:%(name)s:%(levelname)s - %(message)s',datefmt='%y-%m-%d %H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    obt_archivo(logger)
