import graphene
from graphene_django.forms.types import ErrorType

from cep.models import MapFields
from cep.queries.map_fields import MapFieldsNode
from cep.forms import MapFieldsForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    map_fields = graphene.Field(MapFieldsNode)

    class Meta:
        generic_name = 'map_field'
        form_class = MapFieldsForm
        model = MapFields

    @classmethod
    def perform_create(cls, create_cls, form, info):
        map_field = form.save()
        return create_cls(errors=[], map_field=map_field)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        map_field = form.save()
        return update_cls(errors=[], map_field=map_field)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        map_field = instance.delete()
        return delete_cls(errors=[], map_field=map_field)