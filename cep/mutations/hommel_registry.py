import graphene
from graphene_django.forms.types import ErrorType

from cep.models import HommelRegistry
from cep.queries.hommel_registry import HommelRegistryNode
from cep.forms import HommelRegistryForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    hommel_registry = graphene.Field(HommelRegistryNode)

    class Meta:
        generic_name = 'hommel_registry'
        form_class = HommelRegistryForm
        model = HommelRegistry

    @classmethod
    def perform_create(cls, create_cls, form, info):
        hommel_registry = form.save()
        return create_cls(errors=[], hommel_registry=hommel_registry)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        hommel_registry = form.save()
        return update_cls(errors=[], hommel_registry=hommel_registry)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        hommel_registry = instance.delete()
        return delete_cls(errors=[], hommel_registry=hommel_registry)