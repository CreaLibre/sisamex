import graphene
from graphene_django.forms.types import ErrorType

from cep.models import Machine
from cep.queries.machine import CepMachineNode
from cep.forms import MachineForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    cep_machine = graphene.Field(CepMachineNode)

    class Meta:
        generic_name = 'cep_machine'
        form_class = MachineForm
        model = Machine

    @classmethod
    def perform_create(cls, create_cls, form, info):
        cep_machine = form.save()
        return create_cls(errors=[], cep_machine=cep_machine)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        cep_machine = form.save()
        return update_cls(errors=[], cep_machine=cep_machine)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        cep_machine = instance.delete()
        return delete_cls(errors=[], cep_machine=cep_machine)