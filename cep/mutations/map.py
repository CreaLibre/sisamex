import graphene
from graphene_django.forms.types import ErrorType

from cep.models import Map
from cep.queries.map import MapNode
from cep.forms import MapForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    map = graphene.Field(MapNode)

    class Meta:
        generic_name = 'map'
        form_class = MapForm
        model = Map

    @classmethod
    def perform_create(cls, create_cls, form, info):
        map = form.save()
        return create_cls(errors=[], map=map)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        map = form.save()
        return update_cls(errors=[], map=map)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        map = instance.delete()
        return delete_cls(errors=[], map=map)