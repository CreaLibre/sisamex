import graphene
from graphene_django.forms.types import ErrorType

from cep.models import File
from cep.queries.file import FileNode
from cep.forms import FileForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    file = graphene.Field(FileNode)

    class Meta:
        generic_name = 'file'
        form_class = FileForm
        model = File

    @classmethod
    def perform_create(cls, create_cls, form, info):
        file = form.save()
        return create_cls(errors=[], file=file)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        file = form.save()
        return update_cls(errors=[], file=file)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        file = instance.delete()
        return delete_cls(errors=[], file=file)