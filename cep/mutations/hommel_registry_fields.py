import graphene
from graphene_django.forms.types import ErrorType

from cep.models import HommelRegistryFields
from cep.queries.hommel_registry_fields import HommelRegistryFieldsNode
from cep.forms import HommelRegistryFieldsForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    hommel_registry_fields = graphene.Field(HommelRegistryFieldsNode)

    class Meta:
        generic_name = 'hommel_registry_field'
        form_class = HommelRegistryFieldsForm
        model = HommelRegistryFields

    @classmethod
    def perform_create(cls, create_cls, form, info):
        hommel_registry_fields = form.save()
        return create_cls(errors=[], hommel_registry_fields=hommel_registry_fields)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        hommel_registry_fields = form.save()
        return update_cls(errors=[], hommel_registry_fields=hommel_registry_fields)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        hommel_registry_fields = instance.delete()
        return delete_cls(errors=[], hommel_registry_fields=hommel_registry_fields)