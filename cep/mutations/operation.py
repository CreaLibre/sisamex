import graphene
from graphene_django.forms.types import ErrorType

from cep.models import Operation
from cep.queries.operation import OperationNode
from cep.forms import OperationForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    operation = graphene.Field(OperationNode)

    class Meta:
        generic_name = 'operation'
        form_class = OperationForm
        model = Operation

    @classmethod
    def perform_create(cls, create_cls, form, info):
        operation = form.save()
        return create_cls(errors=[], operation=operation)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        operation = form.save()
        return update_cls(errors=[], operation=operation)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        operation = instance.delete()
        return delete_cls(errors=[], operation=operation)