import graphene
from graphene_django.forms.types import ErrorType

from cep.models import SpecificationLimit
from cep.queries.specification_limit import SpecificationLimitNode
from cep.forms import SpecificationLimitForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    specification_limit = graphene.Field(SpecificationLimitNode)

    class Meta:
        generic_name = 'specification_limit'
        form_class = SpecificationLimitForm
        model = SpecificationLimit

    @classmethod
    def perform_create(cls, create_cls, form, info):
        specification_limit = form.save()
        return create_cls(errors=[], specification_limit=specification_limit)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        specification_limit = form.save()
        return update_cls(errors=[], specification_limit=specification_limit)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        specification_limit = instance.delete()
        return delete_cls(errors=[], specification_limit=specification_limit)