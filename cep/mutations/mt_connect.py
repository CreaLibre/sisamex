import graphene
from graphene_django.forms.types import ErrorType

from cep.models import MtConnect
from cep.queries.mt_connect import MtConnectNode
from cep.forms import MtConnectForm
from base.graphene.mutations import GenericMutations

class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    mt_connect = graphene.Field(MtConnectNode)

    class Meta:
        generic_name = 'mt_connect'
        form_class = MtConnectForm
        model = MtConnect

    @classmethod
    def perform_create(cls, create_cls, form, info):
        mt_connect = form.save()
        return create_cls(errors=[], mt_connect=mt_connect)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        mt_connect = form.save()
        return update_cls(errors=[], mt_connect=mt_connect)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        mt_connect = instance.delete()
        return delete_cls(errors=[], mt_connect=mt_connect)