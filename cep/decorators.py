from organization.models import LocalBusiness
from .models.machines import Machine
from .models.hommel_registry import HommelRegistry
from django.db.models import Q

def allowed_local_business(func):
    def wrap(*args, **kwargs):
        result = func(*args, **kwargs)
        user = args[1].context.user

        plants = LocalBusiness.objects.none()
        machines = Machine.objects.none()
        for group in user.groups_ref.prefetch_related('maintenance_plants'):
            plants = plants | group.maintenance_plants.filter(
                Q(role="operations_user") |
                Q(role="operations_supervisor") |
                Q(role="quality_user") |
                Q(role="quality_supervisor"))
            if (group.role == "operations_user" or group.role == "operations_supervisor" or
                group.role == "quality_user" or group.role == "quality_supervisor"):
                machines = machines | Machine.objects.filter(maintenance_plant__in=list(plants))

        # if model_type = Model:
        #     result = result.filter(foo=bar)
        # else:
        #     result = result.filter(maintenance_plant__in=plants)

        return machines
    return wrap


def allowed_local_business_hr(func):
    def wrap(*args, **kwargs):
        result = func(*args, **kwargs)
        user = args[1].context.user

        plants = LocalBusiness.objects.none()
        hr = HommelRegistry.objects.none()
        for group in user.groups_ref.prefetch_related('maintenance_plants'):
            plants = plants | group.maintenance_plants.all()
            if (group.role == "operations_user" or group.role == "operations_supervisor" or
                group.role == "quality_user" or group.role == "quality_supervisor"):
                hr = hr | HommelRegistry.objects.filter(machine__maintenance_plant__in=list(plants))

        # if model_type = Model:
        #     result = result.filter(foo=bar)
        # else:
        #     result = result.filter(maintenance_plant__in=plants)

        return hr
    return wrap
        