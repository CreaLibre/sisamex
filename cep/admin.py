from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import (
    File,
    Operation,
    HommelRegistry,
    HommelRegistryFields,
    Map,
    MapFields,
    SpecificationLimit,
    Machine,
    CepHistoricalAlert,
    MtConnect
)

@admin.register(File)
class FileAdmin(ImportExportModelAdmin):
    pass

@admin.register(Operation)
class OperationAdmin(ImportExportModelAdmin):
    pass

@admin.register(HommelRegistry)
class HommelRegistryAdmin(ImportExportModelAdmin):
    pass

@admin.register(HommelRegistryFields)
class HommelRegistryFieldsAdmin(ImportExportModelAdmin):
    pass

@admin.register(Map)
class MapAdmin(ImportExportModelAdmin):
    pass

@admin.register(MapFields)
class MapFieldsAdmin(ImportExportModelAdmin):
    pass

@admin.register(SpecificationLimit)
class SpecificationLimitAdmin(ImportExportModelAdmin):
    list_display = ['name', 'upper_limit', 'lower_limit', 'nominal', 'number_part']
    readonly_fields = ('number_part',)

# @admin.register(Machine)
# class MachineAdmin(ImportExportModelAdmin):
#     pass

@admin.register(CepHistoricalAlert)
class CepHistoricalAlertAdmin(ImportExportModelAdmin):
    pass

@admin.register(MtConnect)
class MtConnectAdmin(ImportExportModelAdmin):
    pass