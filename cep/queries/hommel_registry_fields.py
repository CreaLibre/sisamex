import graphene
from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from django.db.models import F, Q, FloatField
from django.db.models.functions import Cast

from cep.models import HommelRegistryFields
from base.utils import optimize_query

class HommelRegistryFieldsNode(DjangoObjectType):
    class Meta:
        model = HommelRegistryFields
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    status = graphene.String()

    def resolve_status(self, info, **kwargs):
        specification_limit = self.map_fields.specificationlimit_set.all().first()
        try:
            if self.value_field < float(specification_limit.lower_limit) or self.value_field > float(specification_limit.upper_limit):
                return "ALERT"
            else:
                return "OK"
        except:
            return "Map Field has no Specification Limit relation, no alert calculation can be made"


class Query(AbstractType):
    hommel_registry_field = relay.Node.Field(HommelRegistryFieldsNode)

    all_hommel_registry_fields = DjangoFilterConnectionField(
        HommelRegistryFieldsNode,
        status=graphene.String()
    )

    @login_required
    def resolve_all_hommel_registry_fields(self, info, status=None, **kwargs):
        if status is None:
            return optimize_query(HommelRegistryFields.objects.all(), info)
        else:
            if status == "ALERT":
                import pdb; pdb.set_trace()
                return optimize_query(HommelRegistryFields.objects.filter(
                    specificationlimit__first__gte=F('value_field')
                    ), info)
