from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import Operation
from base.utils import optimize_query

class OperationNode(DjangoObjectType):
    class Meta:
        model = Operation
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    operation = relay.Node.Field(OperationNode)

    all_operations = DjangoFilterConnectionField(
        OperationNode,
    )

    @login_required
    def resolve_all_operations(self, info, **kwargs):
        return optimize_query(Operation.objects.all(), info)