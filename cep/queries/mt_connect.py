from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import MtConnect
from base.utils import optimize_query

class MtConnectNode(DjangoObjectType):
    class Meta:
        model = MtConnect
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    mt_connect = relay.Node.Field(MtConnectNode)

    all_mt_connect = DjangoFilterConnectionField(
        MtConnectNode,
    )

    @login_required
    def resolve_all_mt_connect(self, info, **kwargs):
        return optimize_query(MtConnect.objects.all(), info)