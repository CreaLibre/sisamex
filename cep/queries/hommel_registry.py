from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import HommelRegistry
from base.utils import optimize_query
from cep.decorators import allowed_local_business_hr

class HommelRegistryNode(DjangoObjectType):
    class Meta:
        model = HommelRegistry
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    hommel_registry = relay.Node.Field(HommelRegistryNode)

    all_hommel_registrys = DjangoFilterConnectionField(
        HommelRegistryNode,
    )

    @login_required
    @allowed_local_business_hr
    def resolve_all_hommel_registrys(self, info, **kwargs):
        return optimize_query(HommelRegistry.objects.all(), info)