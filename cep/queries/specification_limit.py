from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import SpecificationLimit
from base.utils import optimize_query

class SpecificationLimitNode(DjangoObjectType):
    class Meta:
        model = SpecificationLimit
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    specification_limit = relay.Node.Field(SpecificationLimitNode)

    all_specification_limits = DjangoFilterConnectionField(
        SpecificationLimitNode,
    )

    @login_required
    def resolve_all_specification_limits(self, info, **kwargs):
        return optimize_query(SpecificationLimit.objects.all(), info)