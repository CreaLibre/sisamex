from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import CepHistoricalAlert
from base.utils import optimize_query

class CepHistoricalAlertNode(DjangoObjectType):
    class Meta:
        model = CepHistoricalAlert
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    cep_historical_alert = relay.Node.Field(CepHistoricalAlertNode)

    all_cep_historical_alerts = DjangoFilterConnectionField(
        CepHistoricalAlertNode,
    )

    @login_required
    def resolve_all_cep_historical_alerts(self, info, **kwargs):
        return optimize_query(CepHistoricalAlert.objects.all(), info)