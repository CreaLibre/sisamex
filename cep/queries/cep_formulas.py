from datetime import datetime

import graphene
from graphene import relay, AbstractType

from graphql_jwt.decorators import login_required

from cep.utils.cep_formulas import cep_formulas, cep_formulas_query

from cep.models import MapFields

from cep.utils.utils import get_object_by_relay_id

class QueryCepFormulas(graphene.ObjectType):
    date = graphene.Date()
    lsc = graphene.Float()
    lic = graphene.Float()
    st_dev = graphene.Float()
    cp = graphene.Float()
    cpu = graphene.Float()
    cpl = graphene.Float()
    cpk = graphene.Float()
    prom = graphene.Float()
    ucl = graphene.Float()
    lcl = graphene.Float()

class XRegistries(graphene.ObjectType):
    idx = graphene.Int()
    id = graphene.Int()
    name_id = graphene.Int()
    name = graphene.String()
    value = graphene.Float()
    serial_number = graphene.String()
    date = graphene.Date()
    hour = graphene.Time()
    x_sub_x = graphene.Float()
    pow2_x_sub_x = graphene.Float()

class CepFormulas(graphene.ObjectType):
    lsc = graphene.Float()
    lic = graphene.Float()
    st_dev = graphene.Float()
    cp = graphene.Float()
    cpu = graphene.Float()
    cpl = graphene.Float()
    cpk = graphene.Float()
    ucl = graphene.Float()
    lcl = graphene.Float()
    prom = graphene.Float()
    xregistries = graphene.List(XRegistries)

class CepFormulasResult(graphene.ObjectType):
    cep_formulas = graphene.List(CepFormulas)

class QueryCepFormulasResult(graphene.ObjectType):
    cep_formulas = graphene.List(QueryCepFormulas)

class Query(AbstractType):
    cep_formulas_query = graphene.Field(
        CepFormulasResult,
        map_field_id=graphene.String(),
        cantidad_registros=graphene.Int(),
        usl=graphene.Float(),
        lsl=graphene.Float(),
        numero_parte=graphene.String(),
        caracteristica_id=graphene.Int(),
        operacion=graphene.String(),
        maquina=graphene.String()
    )

    cep_formulas_sql_query = graphene.Field(
        QueryCepFormulasResult,
        map_field_id=graphene.String(),
        cantidad_registros=graphene.Int(),
        usl=graphene.Float(),
        lsl=graphene.Float(),
        numero_parte=graphene.String(),
        caracteristica_id=graphene.Int(),
        operacion=graphene.String(),
        maquina=graphene.String()
    )

    @login_required
    def resolve_cep_formulas_sql_query(
        self, info,
        cantidad_registros=None,
        usl=None, lsl=None,
        numero_parte=None,
        operacion=None,
        maquina=None,
        map_field_id=None,
        caracteristica_id=None
    ):
        if map_field_id is None:
            mp_id = caracteristica_id
        else:
            mp_id = map_field_id

        results = cep_formulas_query(
            mp_id,
            cantidad_registros,
            usl, lsl,
            numero_parte,
            operacion,
            maquina
        )

        cep_formulas_result = []
        if usl is None or lsl is None:
            for result in results:
                cep_formulas_result.append(QueryCepFormulas(
                    date=result['FECHA'],
                    lsc=result['LSC'],
                    lic=result['LIC'],
                    st_dev=result['StDev'],
                    cp=float(0.0),
                    cpu=result['CPKU'],
                    cpl=result['CPKL'],
                    cpk=result['CPK'],
                    prom=result['PROMEDIO'],
                    ucl=result['UCL'],
                    lcl=result['LCL']
                ))
        else:
            for result in results:
                cep_formulas_result.append(QueryCepFormulas(
                    date=result['FECHA'],
                    lsc=result['LSC'],
                    lic=result['LIC'],
                    st_dev=result['StDev'],
                    cp=result['CP'],
                    cpu=result['CPKU'],
                    cpl=result['CPKL'],
                    cpk=result['CPK'],
                    prom=result['PROMEDIO'],
                    ucl=result['UCL'],
                    lcl=result['LCL']
                ))
        return QueryCepFormulasResult(cep_formulas=cep_formulas_result)

    @login_required
    def resolve_cep_formulas_query(
        self, info,
        cantidad_registros,
        usl,
        lsl,
        numero_parte,
        operacion,
        maquina,
        map_field_id=None,
        caracteristica_id=None,
        **kwargs
    ):

        if map_field_id is None:
            mp_id = caracteristica_id
        else:
            mp_id = get_object_by_relay_id(MapFields, map_field_id).id

        results = cep_formulas(
            map_field_id,
            cantidad_registros,
            usl, lsl,
            numero_parte,
            mp_id,
            operacion,
            maquina
        )

        cep_formulas_result = []
        for result in results:
            xregistries = []
            for xregistry in result['xregistros'][0]:
                xregistries.append(XRegistries(
                    idx=xregistry['idx'],
                    id=xregistry['id'],
                    name_id=xregistry['nombre_id'],
                    name=xregistry['nombre'],
                    value=xregistry['valor'],
                    serial_number=xregistry['numero_serie'],
                    date=xregistry['fecha'],
                    hour=xregistry['hora'],
                    x_sub_x=xregistry['x-x'],
                    pow2_x_sub_x=xregistry['(x-x)2']
                ))
            cep_formulas_result.append(CepFormulas(
                lsc=result['LSC'],
                lic=result['LIC'],
                st_dev=result['StDev'],
                cp=result['CP'],
                cpu=result['CPU'],
                cpl=result['CPL'],
                cpk=result['CPK'],
                ucl=result['UCL'],
                lcl=result['LCL'],
                prom=result['PROM'],
                xregistries=xregistries
            ))
        return CepFormulasResult(cep_formulas=cep_formulas_result)