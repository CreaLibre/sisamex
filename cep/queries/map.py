from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import Map
from base.utils import optimize_query

class MapNode(DjangoObjectType):
    class Meta:
        model = Map
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    map = relay.Node.Field(MapNode)

    all_maps = DjangoFilterConnectionField(
        MapNode,
    )

    @login_required
    def resolve_all_maps(self, info, **kwargs):
        return optimize_query(Map.objects.all(), info)