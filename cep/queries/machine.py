import graphene
from graphene import relay, AbstractType
from django.db import connection

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import Machine, MapFields
from base.utils import optimize_query
from cep.decorators import allowed_local_business

class CepMachineNode(DjangoObjectType):
    class Meta:
        model = Machine
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    alerts = graphene.Int()

    def resolve_alerts(self, info, **kwargs):
        with connection.cursor() as cursor:
            cursor.execute("SELECT count(*) FROM CEP_alerts_by_machine(%s);", [self.pk])
            row = cursor.fetchone()
        return row[0]
        # map_fields = MapFields.objects.filter(hommelregistryfields__registry__machine=self)
        # alerts = 0
        # for map_field in map_fields:
        #     value = map_field.hommelregistryfields_set.all().order_by('registry__date_and_time').first()
        #     if value.status == 'ALERT':
        #         alerts += 1
        # return alerts

class Query(AbstractType):
    cep_machine = relay.Node.Field(CepMachineNode)

    all_cep_machines = DjangoFilterConnectionField(
        CepMachineNode,
    )

    @login_required
    @allowed_local_business
    def resolve_all_cep_machines(self, info, **kwargs):
        return optimize_query(Machine.objects.all(), info)