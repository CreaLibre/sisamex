import graphene
from graphene import relay, AbstractType

from datetime import datetime, timedelta

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import MapFields, HommelRegistry, HommelRegistryFields
from base.utils import optimize_query

from cep.utils.cep_formulas import hommel_map_field_formulas
from cep.utils.utils import get_object_by_relay_id
from cep.queries.hommel_registry import HommelRegistryNode
from cep.queries.hommel_registry_fields import HommelRegistryFieldsNode

class MapFieldsNode(DjangoObjectType):
    class Meta:
        model = MapFields
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    hommel_registry_graph = DjangoFilterConnectionField(HommelRegistryFieldsNode)

    def resolve_hommel_registry_graph(self, info, **kwargs):
        return optimize_query(HommelRegistryFields.objects.filter(map_fields=self).order_by("-registry__date_and_time")[:10], info)

class CepFormulaPerMapField(graphene.ObjectType):
    map_field = graphene.Field(MapFieldsNode)
    lsc = graphene.Float()
    lic = graphene.Float()
    st_dev = graphene.Float()
    cp = graphene.Float()
    cpu = graphene.Float()
    cpl = graphene.Float()
    cpk = graphene.Float()
    lse = graphene.Float()
    lie = graphene.Float()
    prom = graphene.Float()
    nom = graphene.Float()

class Query(AbstractType):
    map_field = relay.Node.Field(MapFieldsNode)

    all_map_fields = DjangoFilterConnectionField(
        MapFieldsNode,
    )

    cep_formula_per_map_field = graphene.List(
        CepFormulaPerMapField,
        numero_parte=graphene.String(),
        maquina=graphene.String(),
        operacion=graphene.String(),
        cantidad_registros=graphene.Int(),
        map_field_id=graphene.String()
    )

    @login_required
    def resolve_all_map_fields(self, info, **kwargs):
        return optimize_query(MapFields.objects.all(), info)

    @login_required
    def resolve_cep_formula_per_map_field(self, info, numero_parte, maquina, operacion, cantidad_registros=None, map_field_id=None, **kwargs):
        if cantidad_registros is None:
            cantidad_registros = 35
        map_fields = []
        if map_field_id is None:
            map_field_objects = MapFields.objects.all()
        else:
            map_field_objects = MapFields.objects.filter(pk=get_object_by_relay_id(MapFields, map_field_id).pk)
        for map_field in map_field_objects:
            result = hommel_map_field_formulas(map_field, numero_parte, maquina, operacion, cantidad_registros)
            map_fields.append(CepFormulaPerMapField(
                map_field=map_field,
                lsc=result['LSC'],
                lic=result['LIC'],
                st_dev=result['StDev'],
                cp=result['CP'],
                cpu=result['CPU'],
                cpl=result['CPL'],
                cpk=result['CPK'],
                lse=result['LSE'],
                lie=result['LIE'],
                prom=result['PROM'],
                nom=result['NOM']
            ))
        return map_fields
            