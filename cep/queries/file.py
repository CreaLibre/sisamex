import graphene
from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from cep.models import File
from organization.models import Machine
from base.utils import optimize_query

from .hommel_registry import HommelRegistryNode
from organization.queries.machine import MachineNode
class FileNode(DjangoObjectType):
    class Meta:
        model = File
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    machines = DjangoFilterConnectionField(MachineNode, machine_name=graphene.String())

    hommelregistry_set = DjangoFilterConnectionField(HommelRegistryNode, machine_name=graphene.String())

    def resolve_machines(self, info, machine_name=None, **kwargs):
        if machine_name is None:
            return optimize_query(Machine.objects.filter(hommelregistry__file=self).order_by('name').distinct('name'), info)
        else:
            return optimize_query(Machine.objects.filter(hommelregistry__file=self, name=machine_name).order_by('name').distinct('name'), info)

    def resolve_hommelregistry_set(self, info, machine_name=None, **kwargs):
        if machine_name is None:
            return optimize_query(self.hommelregistry_set.all(), info)
        else:
            return optimize_query(self.hommelregistry_set.filter(machine__name=machine_name), info)


class Query(AbstractType):
    file = relay.Node.Field(FileNode)

    all_files = DjangoFilterConnectionField(
        FileNode,
    )

    @login_required
    def resolve_all_files(self, info, **kwargs):
        return optimize_query(File.objects.all(), info)