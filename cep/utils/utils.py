from graphql_relay.node.node import from_global_id

def get_object_by_relay_id(Model, id_):
    return Model.objects.get(pk=int(from_global_id(id_)[1]))

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]