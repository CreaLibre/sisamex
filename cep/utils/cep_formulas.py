import math, pyodbc, os

from django.db.models import Max
from django.db import connections

from .utils import dictfetchall, get_object_by_relay_id

from cep.models import (
    File,
    Operation,
    HommelRegistry,
    HommelRegistryFields,
    Map,
    MapFields,
    SpecificationLimit
)

def map_field_name(pk):
    try:
        map_field = get_object_by_relay_id(MapFields, pk)
    except:
        return None
    return map_field.field_name

def cep_formulas_query(
    map_field_id=None,
    cantidad_registros=None,
    usl=None, lsl=None,
    numero_parte=None,
    operacion=None,
    maquina=None
):
    server = f"{os.environ.get('DAIMLER_DB_HOST')},{os.environ.get('DAIMLER_DB_PORT')}"
    database = os.environ.get('DAIMLER_DB_NAME')
    username =  os.environ.get('DAIMLER_DB_USER')
    password = os.environ.get('DAIMLER_DB_PASSWORD')
    daimler_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    if usl is None or lsl is None:
        query = '''
            SELECT STDGESAMT.ERSTELLDATUM AS FECHA,TEIL as Parte, PRUEFPLAN AS PlanInspeccion , VORGANGBEZ  AS Maquina, MERKMALBEZ AS IDCharacteristrics, NOMINAL, OT AS LSC,UT AS LIC,MINIMUM AS LIE, MAXIMUM AS LSE, XQ AS PROMEDIO,SIGMA AS StDev,CP,CPK,CPKU,CPKL,
            XQ + (3 * SIGMA) AS UCL, XQ - (3 * SIGMA) AS LCL
            FROM STDGESAMTDETAIL
            INNER JOIN STDGESAMT ON STDGESAMTDETAIL.NR = STDGESAMT.NR
            WHERE STDGESAMT.BEZEICHNUNG = 'Analytics'
            AND GEWM >= 3
        '''
    else:
        query = '''
            SELECT STDGESAMT.ERSTELLDATUM AS FECHA,TEIL as Parte, PRUEFPLAN AS PlanInspeccion , VORGANGBEZ  AS Maquina, MERKMALBEZ AS IDCharacteristrics, NOMINAL, OT AS LSC,UT AS LIC,MINIMUM AS LIE, MAXIMUM AS LSE, XQ AS PROMEDIO,SIGMA AS StDev,CP,CPK,CPKU,CPKL,
            XQ + (3 * SIGMA) AS UCL, XQ - (3 * SIGMA) AS LCL, ({usl} - {lsl}) / (6 * SIGMA) AS CP
            FROM STDGESAMTDETAIL
            INNER JOIN STDGESAMT ON STDGESAMTDETAIL.NR = STDGESAMT.NR
            WHERE STDGESAMT.BEZEICHNUNG = 'Analytics'
            AND GEWM >= 3
        '''.format(usl=usl, lsl=lsl)
    if map_field_id is not None:
        var_name = map_field_name(map_field_id)
        if var_name is not None:
            query += " AND MERKMALBEZ LIKE '{}'".format(var_name)
    if maquina is not None and isinstance(maquina, str):
        if '-' in maquina:
            machine_split = maquina.split('-')
            maquina = '{}_{}'.format(machine_split[0], machine_split[1])
        query += " AND VORGANGBEZ = '{}'".format(maquina)
    if numero_parte is not None and isinstance(numero_parte, str):
        query += " AND TEIL LIKE '%{}'".format(numero_parte)
    if operacion is not None and isinstance(operacion, str):
        pass
    if usl is not None:
        pass
    if lsl is not None:
        pass
    if cantidad_registros is not None:
        query += " LIMIT {}".format(cantidad_registros)
    result = []
    with daimler_conn.cursor() as cursor:
        cursor.execute(query)
        result = dictfetchall(cursor)
    return result

def cep_formulas(
    cantidad_registros,
    usl, lsl,
    numero_parte,
    caracteristica_id,
    operacion,
    maquina
):
    r_out = []
    x_sum = 0.0
    x_promedio = 0.0
    x_sigma = 0.0
    x_cp = 0.0
    x_cpu = 0.0
    x_cpl = 0.0
    x_cpk1 = 0.0
    x_cpk2 = 0.0
    x_usl = float(usl)
    x_lsl = float(lsl)
    if numero_parte is not None and len(numero_parte) > 0 and operacion is not None and len(operacion) > 0:
        ultimo_arch = File.objects.filter(
            number_part = numero_parte,
            operation__name = operacion
        ).aggregate(Max('name')).get('name__max',0)
        arch = File.objects.get(name = ultimo_arch)
        if arch:
            _mapaCampos = MapFields.objects.get(id=caracteristica_id)
            _registros = HommelRegistry.objects.select_related().filter(
                file = arch,
                measure_type ='Produccion',
                machine__name = maquina #'DR-1014'
            ).order_by('-date_and_time')[:cantidad_registros]
            _registroDetalle = HommelRegistryFields.objects.select_related().filter(
                registry__in = _registros,
                map_fields = _mapaCampos,
            )
            #obtiene promedio
            cantidad_registros = 0
            for detalle in _registroDetalle:
                if detalle.value_field > 0.0:
                    cantidad_registros += 1
                    x_sum += detalle.value_field

            x_promedio = float(x_sum) / float(cantidad_registros)

            #obtiene sigma
            reg_det=[]
            idx = 0
            sum2 = 0.0
            for detalle in _registroDetalle:
                if detalle.value_field > 0.0:
                    idx += 1
                    r_det = {
                        'idx' : idx,
                        'id' : detalle.id,
                        'nombre_id' : detalle.map_fields.id ,
                        'nombre' : detalle.map_fields.field_name,
                        'valor' : detalle.value_field,
                        'numero_serie': detalle.registry.serial_number,
                        'fecha': detalle.registry.date_and_time.date(),
                        'hora': detalle.registry.date_and_time.time(),
                        'x-x': detalle.value_field - x_promedio,
                        '(x-x)2': pow(detalle.value_field - x_promedio,2)
                    }
                    sum2 += pow(detalle.value_field - x_promedio,2)
                    reg_det.append(r_det)

            x_sigma = math.sqrt(sum2 / float(idx - 1))

            x_ucl = x_promedio + (3 * x_sigma)

            x_lcl = x_promedio - (3 * x_sigma)

            x_cp = ( (x_usl - x_lsl) / (float(6) * x_sigma) )

            x_cpu = ( (x_usl - x_promedio) / (float(3) * x_sigma) )

            x_cpl = (x_promedio - x_lsl)/(float(3) * x_sigma)

            x_cpk1 = abs( ( (x_usl - x_promedio) / (float(3) * x_sigma) ) )

            x_cpk2 = abs( (x_promedio - x_lsl) / (float(3) * x_sigma) )

            if x_cpk1 < x_cpk2:
                x_cpk = x_cpk1
            else:
                x_cpk = x_cpk2

            dat = {
                'LSC': usl,
                'LIC': lsl,
                'StDev' : round(x_sigma,8),
                'CP' : round(x_cp,8),
                'CPU' : round(x_cpu,8),
                'CPL' : round(x_cpl,8),
                'CPK' : round(x_cpk,8),
                'UCL' : round(x_ucl,8),
                'LCL' : round(x_lcl,8),
                'PROM': round(x_promedio, 8),
                'xregistros':[]
            }
            dat.get('xregistros').append(reg_det)
            r_out.append(dat)

    return r_out

def formulas(kwargs):
    cantidad_registros = kwargs.get('cantidad_registros')
    usl = kwargs.get('usl')
    lsl = kwargs.get('lsl')
    numero_parte = kwargs.get('numero_parte')
    caracteristica_id = kwargs.get('caracteristica_id')
    operacion = kwargs.get('operacion')
    maquina = kwargs.get('maquina')

    r_out = []
    x_sum = 0.0
    x_promedio = 0.0
    x_sigma = 0.0
    x_cp = 0.0
    x_cpu = 0.0
    x_cpl = 0.0
    x_cpk1 = 0.0
    x_cpk2 = 0.0
    x_usl = float(usl)
    x_lsl = float(lsl)
    
    if numero_parte is not None and len(numero_parte) > 0 and operacion is not None and len(operacion) > 0:
        ultimo_arch = File.objects.filter(
            number_part = numero_parte,
            operation__name = operacion
        ).aggregate(Max('name')).get('name__max',0)
        arch = File.objects.get(name=ultimo_arch)
        if arch:
            _mapaCampos = MapFields.objects.get(id=caracteristica_id)
            _registros = HommelRegistry.objects.select_related().filter(
                file = arch,
                measure_type ='Produccion',
                machine__name = maquina #'DR-1013'
            ).order_by('-date_and_time')[:cantidad_registros]
            _registroDetalle = HommelRegistryFields.objects.select_related().filter(
                registry__in = _registros,
                map_fields = _mapaCampos,
            )
            #obtiene promedio
            cantidad_registros = 0
            for detalle in _registroDetalle:
                if detalle.value_field > 0.0:
                    cantidad_registros += 1
                    x_sum += detalle.value_field

            x_promedio = float(x_sum) / float(cantidad_registros)

            #obtiene sigma
            reg_det=[]
            idx = 0
            sum2 = 0.0
            for detalle in _registroDetalle:
                if detalle.value_field > 0.0:
                    idx += 1
                    r_det = {
                        'idx' : idx,
                        'id' : detalle.id,
                        'nombre_id' : detalle.map_fields.id ,
                        'nombre' : detalle.map_fields.field_name,
                        'valor' : detalle.value_field,
                        'numero_serie': detalle.registry.serial_number,
                        'fecha_hora': detalle.registry.date_and_time,
                        'x-x': detalle.value_field - x_promedio,
                        '(x-x)2:': pow(detalle.value_field - x_promedio,2)
                    }
                    sum2 += pow(detalle.value_field - x_promedio,2)
                    reg_det.append(r_det)

            x_sigma = math.sqrt(sum2 / float(idx - 1))

            x_ucl = x_promedio + (3 * x_sigma)

            x_lcl = x_promedio - (3 * x_sigma)

            x_cp = ( (x_usl - x_lsl) / (float(6) * x_sigma) )

            x_cpu = ( (x_usl - x_promedio) / (float(3) * x_sigma) )

            x_cpl = (x_promedio - x_lsl)/(float(3) * x_sigma)

            x_cpk1 = abs( ( (x_usl - x_promedio) / (float(3) * x_sigma) ) )

            x_cpk2 = abs( (x_promedio - x_lsl) / (float(3) * x_sigma) )

            if x_cpk1 < x_cpk2:
                x_cpk = x_cpk1
            else:
                x_cpk = x_cpk2

            # Section for load upper and lower specification limits
            sl = SpecificationLimit.objects.all()
            LSE = 0
            LIE = 0
            NOM = 0
            for i in sl:
                if (i.nombre_id == caracteristica_id) :
                    LSE = i.upper_limit
                    LIE = i.lower_limit
                    if i.nominal is not None and i.nominal is not '':
                        NOM = i.nominal
            dat = {
                'nombre_id': caracteristica_id,
                # 'usl': usl,
                # 'lsl': lsl,
                # 'suma': round(x_sum,8),
                'PROM': round(x_promedio,8),
                'NOM': round(float(NOM), 8),
                'StDev' : round(x_sigma,8),
                'CP' : round(x_cp,8),
                'CPU' : round(x_cpu,8),
                'CPL' : round(x_cpl,8),
                'CPK' : round(x_cpk,8),
                'LSC' : round(x_ucl,8),
                'LIC' : round(x_lcl,8),
                'LSE' : round(float(LSE),8),
                'LIE' : round(float(LIE),8)
                # 'xregistros':[]
            }
    return dat

def hommel_map_field_formulas(field, number_part, machine, operation, registry_amount=35, time_range=None):
    _carac = SpecificationLimit.objects.filter(map_field=field)
    if _carac.exists():
        m_min = _carac.first().lower_limit
        m_max = _carac.first().upper_limit
    else:
        m_min = 0.0
        m_max = 0.0

    cantidad_registros = registry_amount
    caracteristica_id = field.id
    numero_parte = number_part
    maquina = machine
    operacion = operation
    usl = m_max
    lsl = m_min

    x_sum = 0.0
    x_promedio = 0.0
    x_sigma = 0.0
    x_cp = 0.0
    x_cpu = 0.0
    x_cpl = 0.0
    x_cpk1 = 0.0
    x_cpk2 = 0.0
    x_cpk = 0.0
    x_ucl = 0.0
    x_lcl = 0.0
    x_usl = float(usl)
    x_lsl = float(lsl)

    if numero_parte is not None and len(numero_parte) > 0 and operacion is not None and len(operacion) > 0:
        ultimo_arch = File.objects.filter(
            number_part = numero_parte,
            operation__name = operacion
        ).aggregate(Max('name')).get('name__max',0)
        arch = File.objects.filter(name=ultimo_arch).first()
        if arch:
            _mapaCampos = MapFields.objects.filter(id=caracteristica_id).first()
            if time_range is None: 
                _registros = HommelRegistry.objects.select_related().filter(
                    file = arch,
                    measure_type ='Produccion',
                    machine__name = maquina #'DR-1013'
                ).order_by('-date_and_time')[:cantidad_registros]
            else:
                _registros = time_range.objects.filter(
                    file = arch,
                    measure_type ='Produccion',
                    machine__name = maquina #'DR-1013'
                )
            _registroDetalle = HommelRegistryFields.objects.select_related().filter(
                registry__in = _registros,
                map_fields = _mapaCampos,
            )
            #obtiene promedio
            cantidad_registros = 0
            for detalle in _registroDetalle:
                if detalle.value_field > 0.0:
                    cantidad_registros += 1
                    x_sum += detalle.value_field

            try:
                x_promedio = float(x_sum) / float(cantidad_registros)
            except:
                pass

            #obtiene sigma
            idx = 0
            sum2 = 0.0
            for detalle in _registroDetalle:
                if detalle.value_field > 0.0:
                    idx += 1
                    sum2 += pow(detalle.value_field - x_promedio,2)

            try:
                x_sigma = math.sqrt(sum2 / float(idx - 1))

                x_ucl = x_promedio + (3 * x_sigma)

                x_lcl = x_promedio - (3 * x_sigma)

                x_cp = ( (x_usl - x_lsl) / (float(6) * x_sigma) )

                x_cpu = ( (x_usl - x_promedio) / (float(3) * x_sigma) )

                x_cpl = (x_promedio - x_lsl)/(float(3) * x_sigma)

                x_cpk1 = abs( ( (x_usl - x_promedio) / (float(3) * x_sigma) ) )

                x_cpk2 = abs( (x_promedio - x_lsl) / (float(3) * x_sigma) )

                if x_cpk1 < x_cpk2:
                    x_cpk = x_cpk1
                else:
                    x_cpk = x_cpk2
            except:
                pass

            # Section for load upper and lower specification limits
            sl = SpecificationLimit.objects.all()
            LSE = 0
            LIE = 0
            NOM = 0
            for i in sl:
                if (i.map_field == field) :
                    LSE = i.upper_limit
                    LIE = i.lower_limit
                    if i.nominal is not None and i.nominal is not '':
                        NOM = i.nominal

            dat = {
                'nombre_id': caracteristica_id,
                'PROM': round(x_promedio,8),
                'NOM': round(float(NOM), 8),
                'StDev' : round(x_sigma,8),
                'CP' : round(x_cp,8),
                'CPU' : round(x_cpu,8),
                'CPL' : round(x_cpl,8),
                'CPK' : round(x_cpk,8),
                'LSC' : round(x_ucl,8),
                'LIC' : round(x_lcl,8),
                'LSE' : round(float(LSE),8),
                'LIE' : round(float(LIE),8)
            }
    return dat