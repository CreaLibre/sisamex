from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model

class SpecificationLimit(Model):
    map_field = models.ForeignKey(
        'cep.MapFields',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )
    description = models.CharField(_('Description'), max_length=100, null=True, blank=True)
    upper_limit = models.CharField(_('Upper Limit'), max_length=20, null=True, blank=True)
    lower_limit = models.CharField(_('Lower Limit'), max_length=20, null=True, blank=True)
    nominal = models.CharField(_('Nominal'), max_length=20, null=True, blank=True)

    file = models.ForeignKey(
        'cep.File',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )

    #number_part = models.CharField(_('Number part'), max_length=100, null=True, blank=True)

    @property
    def name(self):
        return "{}".format(self.map_field.field_name)

    @property
    def number_part(self):
        return self.file.number_part

    def __str__(self):
        return "{} limits".format(self.name)

    class Meta:
        verbose_name = _("Specification Limit")
        verbose_name_plural = _("Specification Limits")
        ordering = ['-pk']
        db_table = 'smx_specification_limit'