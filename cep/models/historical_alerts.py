from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model

class CepHistoricalAlert(Model):

    machine = models.ForeignKey(
        "cep.Machine",
        blank=True, null=True,
        on_delete=models.SET_NULL,
        related_name="alerts_historical"
    )

    value = models.FloatField(_("Value"))
    
    full_date = models.DateTimeField(_("Date and Time"))

    hr_field = models.ForeignKey(
        "cep.HommelRegistryFields",
        blank=True, null=True,
        related_name="alerts_historical",
        on_delete=models.CASCADE
    )

    map_field = models.ForeignKey(
        "cep.MapFields",
        blank=True, null=True,
        related_name="alerts_historical",
        on_delete=models.SET_NULL
    )

    @property
    def variable_name(self):
        if self.map_field is not None:
            return self.map_field.field_name
        else:
            return "??????"

    def __str__(self):
        try:
            return "{} - {} - {}".format(self.variable_name, self.value, self.full_date.strftime("%d/%m/%Y"))
        except:
            return "Missing Data! - {} - {}".format(self.value, self.full_date.strftime("%d/%m/%Y"))

    def __save_value(self):
        if self.hr_field is not None:
            self.value = self.hr_field.value_field

    class Meta:
        verbose_name = _('Historical Alerts')
        verbose_name_plural = _('Historical Alerts')
        ordering = ['-pk']

    def save(self, **kwargs):
        self.__save_value()
        super().save(**kwargs)