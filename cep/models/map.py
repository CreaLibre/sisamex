from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from base.models import Model

class Map(Model):
    ''' Mapa del archivo csv, solo sera para las columnas que son las medidas que toma la hommel
        las columnas de los valores empizan en la "H" que debe de ser la No. 8
    '''

    version = models.CharField(_('Version'), max_length=20, null=True, blank=True)
    file = models.ForeignKey(
        'cep.File',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )
    creation_date = models.DateTimeField(_('Creation date'), default=timezone.now)

    def __str__(self):
        return "{} - {}".format(self.file.name, self.version)


    class Meta:
        verbose_name = _("Map")
        verbose_name_plural = _("Maps")
        ordering = ['-pk']
        db_table = 'hommel_mapa'