from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model

class File(Model):
    ''' definicion del archivo '''
    name = models.CharField(_('Name'), max_length=100, null=False, blank=False)
    prefix = models.CharField(_('Prefix'), max_length=3, null=False, blank=False)
    number_part = models.CharField(_('numero parte'), max_length=50, null=False, blank=False)#numero_parte
    operation = models.ForeignKey(
        'cep.Operation',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("File")
        verbose_name_plural = _("Files")
        ordering = ['-pk']
        db_table = 'hommel_archivo'

class Operation(Model):
    ''' operacion '''
    name = models.CharField(_('Name'), max_length=6, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Operation")
        verbose_name_plural = _("Operations")
        ordering = ['-pk']
        db_table = 'hommel_operation'