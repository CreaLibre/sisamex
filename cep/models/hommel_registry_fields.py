import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.template import loader
from django.conf import settings

from base.models import Model

from .historical_alerts import CepHistoricalAlert

class HommelRegistryFields(Model):
    ''' Lectura de los valores de los campos '''
    registry = models.ForeignKey(
        'cep.HommelRegistry',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )
    map_fields =  models.ForeignKey(
        'cep.MapFields',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )
    value_field = models.FloatField(_('Value Field'), blank=True, null=True)

    def __str__(self):
        try:
            return "{}".format(self.registry.serial_number)
        except:
            try:
                return "{}".format(self.map_fields.field_name)
            except:
                return "{}".format(self.value_field)

    @property
    def status(self):
        specification_limit = self.map_fields.specificationlimit_set.all().first()
        try:
            if self.value_field < float(specification_limit.lower_limit) or self.value_field > float(specification_limit.upper_limit):
                return "ALERT"
            else:
                return "OK"
        except:
            return "Map Field has no Specification Limit relation, no alert calculation can be made"

    class Meta:
        verbose_name = _('Hommel Registry Field')
        verbose_name_plural = _('Hommel Registry Fields')
        ordering = ['-pk']
        db_table = 'hommel_registro_campos'

    def save(self, **kwargs):
        super().save(**kwargs)

        s_limit = self.map_fields.specificationlimit_set.all().first()
        try:
            if self.value_field < float(s_limit.lower_limit) or self.value_field > float(s_limit.upper_limit):
                CepHistoricalAlert.objects.create(
                    machine=self.registry.machine,
                    value=self.value_field,
                    full_date=self.registry.date_and_time,
                    hr_field=self,
                    map_field=self.map_fields
                )
            else:
                pass
        except:
            pass

        try:
            specification_limit = self.map_fields.specificationlimit_set.all().first()
            if specification_limit is not None:
                if self.value_field < float(specification_limit.lower_limit) or self.value_field > float(specification_limit.upper_limit):
                    subject = 'CEP ALERTA {}'.format(self.map_fields.field_name)
                    if self.value_field < float(specification_limit.lower_limit):
                        message = ('El valor de la variable {} a superado el limite menor de {} con un valor de {}.'\
                            .format(self.map_fields.field_name, specification_limit.lower_limit, self.value_field))
                    elif self.value_field > float(specification_limit.upper_limit):
                        message = ('El valor de la variable {} a superado el limite mayor de {} con un valor de {}.'\
                            .format(self.map_fields.field_name, specification_limit.upper_limit, self.value_field))
                    email_from = settings.DEFAULT_FROM_EMAIL

                    recipient_list = []
                    supervisors = get_user_model().objects.filter(groups_ref__role__in=['maintenance_supervisor'])
                    mime_to_string = supervisors[0].email
                    recipient_list.append(supervisors[0])
                    for supervisor in supervisors[1:]:
                        recipient_list.append(supervisor.email)
                        mime_to_string = mime_to_string + ',' + supervisor.email
                    msg = MIMEMultipart("alternative")
                    msg["Subject"] = subject
                    msg["From"] = email_from
                    msg["To"] = mime_to_string
                    try:
                        template = loader.get_template('email_template.html')
                    except:
                        template = None
                    html_content = template.render({
                        'subject': subject,
                        'text_content': message
                    })
                    part1 = MIMEText(message, "plain")
                    part2 = MIMEText(html_content, "html")
                    msg.attach(part1)
                    msg.attach(part2)
                    context = ssl.create_default_context()
                    try:
                        with smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT) as server:
                            server.starttls(context=context)
                            server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
                            server.sendmail(
                                email_from, recipient_list, msg.as_string()
                            )
                    except:
                        pass
        except:
            pass