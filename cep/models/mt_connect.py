from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model

class MtConnect(Model):

    machine = models.ForeignKey(
        "organization.Machine",
        on_delete=models.CASCADE,
        related_name="mt_connect",
        null=True, blank=True
    )

    name = models.CharField(_('Name'), max_length=64)

    value = models.FloatField(_('Value'))

    creation_date = models.DateTimeField(_('Creation Date'), blank=True, null=True)