from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model

class HommelRegistry(models.Model):
    ''' Lectura de los datos principales de los archivos'''
    file = models.ForeignKey(
        'cep.File',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )
    date_and_time = models.DateTimeField(_('Date and time'), blank=True, null=True)
    operator = models.CharField(_('Operator'), max_length=20, null=True, blank=True)
    serial_number = models.CharField(_('Serial Number'), max_length=100, null=True, blank=True)
    colada = models.CharField(_('Colada'), max_length=20, null=True, blank=True)#colada?
    measure_type = models.CharField(_('Measure Type'), max_length=50, null=True, blank=True)
    machine = models.ForeignKey( #models.CharField(_('Machine'), max_length=50, null=True, blank=True)
        'organization.Machine',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return "{} - {}".format(self.serial_number, self.date_and_time.strftime('%d/%m/%Y %H:%M'))


    class Meta:
        verbose_name = _('Hommel Registry')
        verbose_name_plural = _('Hommel Registries')
        ordering = ['-pk']
        db_table = 'hommel_registro'