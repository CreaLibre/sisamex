from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model

class Machine(Model):
    maintenance_plant = models.ForeignKey(
        'maintenance.LocalBusiness',
        on_delete=models.CASCADE,
        related_name='cep_machine'
    )
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=64)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Machine')
        verbose_name_plural = _('Machines')
        ordering = ['-pk']