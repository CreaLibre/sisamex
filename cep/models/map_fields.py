from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from base.models import Model

class MapFields(Model):
    ''' Definen los campos para los mapas  '''
    map = models.ForeignKey(
        'cep.Map',
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )
    field_name = models.CharField(_('Field Name'), max_length=150, null=False, blank=False)
    column = models.IntegerField(_('Column'), blank=True, null=True)
    creation_date = models.DateTimeField(_('Creation date'), default=timezone.now)
    validate = models.BooleanField(_('Validate'), blank=True, default=True)

    def __str__(self):
        return "{}-{}".format(self.id, self.field_name)


    class Meta:
        verbose_name = _("Map Field")
        verbose_name_plural = _("Map Fields")
        ordering = ['-pk']
        db_table = 'hommel_mapa_campos'