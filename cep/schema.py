import graphene

from .queries.file import Query as file_query
from .queries.hommel_registry import Query as hommel_registry_query
from .queries.hommel_registry_fields import Query as hommel_registry_fields_query
from .queries.map import Query as map_query
from .queries.map_fields import Query as map_fields_query
from .queries.operation import Query as operation_query
from .queries.cep_formulas import Query as cep_formulas_query
from .queries.specification_limit import Query as specification_limit_query
from .queries.machine import Query as machine_query
from .queries.mt_connect import Query as mt_connect_query
from .queries.historical_alerts import Query as cep_historical_alerts_query

from .mutations.file import Mutation as file_mutation
from .mutations.hommel_registry import Mutation as hommel_registry_mutation
from .mutations.hommel_registry_fields import Mutation as hommel_registry_fields_mutation
from .mutations.map import Mutation as map_mutation
from .mutations.map_fields import Mutation as map_fields_mutation
from .mutations.operation import Mutation as operation_mutation
from .mutations.specification_limit import Mutation as specification_limit_mutation
from .mutations.machine import Mutation as machine_mutation
from .mutations.mt_connect import Mutation as mt_connect_mutation

class Query(
    file_query,
    hommel_registry_query,
    hommel_registry_fields_query,
    map_query,
    map_fields_query,
    operation_query,
    cep_formulas_query,
    specification_limit_query,
    machine_query,
    mt_connect_query,
    cep_historical_alerts_query,
    graphene.AbstractType
):
    pass

class Mutation(
    file_mutation,
    hommel_registry_mutation,
    hommel_registry_fields_mutation,
    map_mutation,
    map_fields_mutation,
    operation_mutation,
    specification_limit_mutation,
    machine_mutation,
    mt_connect_mutation,
    graphene.AbstractType
):
    pass