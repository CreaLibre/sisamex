from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from cep.models import HommelRegistry

class HommelRegistryForm(forms.ModelForm):
    class Meta:
        model = HommelRegistry
        fields = '__all__'