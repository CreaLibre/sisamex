from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from cep.models import MapFields

class MapFieldsForm(forms.ModelForm):
    class Meta:
        model = MapFields
        fields = '__all__'