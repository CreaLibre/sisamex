from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from cep.models import SpecificationLimit

class SpecificationLimitForm(forms.ModelForm):
    class Meta:
        model = SpecificationLimit
        fields = '__all__'