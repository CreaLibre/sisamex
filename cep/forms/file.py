from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from cep.models import File, Operation

class FileForm(forms.ModelForm):
    class Meta:
        model = File
        fields = '__all__'

class OperationForm(forms.ModelForm):
    class Meta:
        model = Operation
        fields = '__all__'