from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from cep.models import MtConnect

class MtConnectForm(forms.ModelForm):
    class Meta:
        model = MtConnect
        fields = '__all__'