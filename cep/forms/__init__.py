from .file import FileForm, OperationForm
from .hommel_registry import HommelRegistryForm
from .hommel_registry_fields import HommelRegistryFieldsForm
from .map import MapForm
from .map_fields import MapFieldsForm
from .specification_limit import SpecificationLimitForm
from .machine import MachineForm
from .mt_connect import MtConnectForm