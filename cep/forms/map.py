from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from cep.models import Map

class MapForm(forms.ModelForm):
    class Meta:
        model = Map
        fields = '__all__'