from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from cep.models import HommelRegistryFields

class HommelRegistryFieldsForm(forms.ModelForm):
    class Meta:
        model = HommelRegistryFields
        fields = '__all__'