from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from organization.models import(
    Machine,
    Organization,
    LocalBusiness
)


@admin.register(Machine)
class MachineAdmin(ImportExportModelAdmin):
    list_display = ['name', 'maintenance_plant']
    list_filter = ('maintenance_plant',)
    list_editable = ('maintenance_plant',)


@admin.register(Organization)
class OrganizationAdmin(ImportExportModelAdmin):
    pass


@admin.register(LocalBusiness)
class LocalBusinessAdmin(ImportExportModelAdmin):
    list_display = ['name', 'organization', 'maintenance_warnings', 'maintenance_alerts']
    readonly_fields = ('maintenance_warnings', 'maintenance_alerts',)
