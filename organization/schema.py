import graphene

from .queries.machine import Query as machines_query
from .queries.organization import Query as organizations_query
from .queries.local_business import Query as local_business_query

from .mutations.machine import Mutation as machines_mutation
from .mutations.organization import Mutation as organizations_mutation
from .mutations.local_business import Mutation as local_business_mutation


class Query(
    machines_query,
    organizations_query,
    local_business_query,
    graphene.AbstractType
):
    pass

class Mutation(
    machines_mutation,
    local_business_mutation,
    organizations_mutation,
    graphene.AbstractType,
):
    pass