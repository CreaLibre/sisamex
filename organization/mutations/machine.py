import graphene
from graphene_django.forms.types import ErrorType

from organization.models import Machine
from organization.queries.machine import MachineNode
from organization.forms.machine import MachineForm
from base.graphene.mutations import GenericMutations


class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    machine = graphene.Field(MachineNode)

    class Meta:
        generic_name = 'machine'
        form_class = MachineForm
        model = Machine

    @classmethod
    def perform_create(cls, create_cls, form, info):
        machine = form.save()
        return create_cls(errors=[], machine=machine)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        machine = form.save()
        return update_cls(errors=[], machine=machine)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        machine = instance.delete()
        return delete_cls(errors=[], machine=machine)