import graphene
from graphene_django.forms.types import ErrorType

from organization.models import Organization
from organization.queries.organization import OrganizationNode
from organization.forms.organization import OrganizationForm
from base.graphene.mutations import GenericMutations


class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    organization = graphene.Field(OrganizationNode)

    class Meta:
        generic_name = 'organization'
        form_class = OrganizationForm
        model = Organization

    @classmethod
    def perform_create(cls, create_cls, form, info):
        organization = form.save()
        return create_cls(errors=[], organization=organization)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        organization = form.save()
        return update_cls(errors=[], organization=organization)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        organization = instance.delete()
        return delete_cls(errors=[], organization=organization)