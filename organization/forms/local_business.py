from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from organization.models import LocalBusiness


class LocalBusinessForm(forms.ModelForm):
    class Meta:
        model = LocalBusiness
        fields = '__all__'