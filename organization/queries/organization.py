from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField

from organization.models import Organization
from base.utils import optimize_query

class OrganizationNode(DjangoObjectType):
    class Meta:
        model = Organization
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    organization = relay.Node.Field(OrganizationNode)

    all_organizations = DjangoFilterConnectionField(
        OrganizationNode,
    )

    def resolve_all_organizations(self, info, **kwargs):
        return optimize_query(Organization.objects.all(), info)