import graphene
from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from organization.models import LocalBusiness
from base.utils import optimize_query
from .machine import MachineNode

from ..decorators import allowed_local_business

class LocalBusinessNode(DjangoObjectType):
    class Meta:
        model = LocalBusiness
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    maintenance_warnings = graphene.Int()
    maintenance_alerts = graphene.Int()
    machine_set = DjangoFilterConnectionField(MachineNode, search=graphene.String())

    def resolve_maintenance_warnings(self, info, **kwargs):
        return self.maintenance_warnings

    def resolve_maintenance_alerts(self, info, **kwargs):
        return self.maintenance_alerts

    def resolve_machine_set(self, info, search=None, **kwargs):
        if search is None:
            return optimize_query(self.machine_set.all(), info)
        else:
            return optimize_query(self.machine_set.filter(name__icontains=search), info)


class Query(AbstractType):
    local_business = relay.Node.Field(LocalBusinessNode)

    all_local_business = DjangoFilterConnectionField(
        LocalBusinessNode,
    )

    @login_required
    @allowed_local_business
    def resolve_all_local_business(self, info, **kwargs):
        return optimize_query(LocalBusiness.objects.all(), info)