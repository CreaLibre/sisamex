import graphene
from graphene import relay, AbstractType
from graphql_relay import to_global_id

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required
from django.db.models import Q

from maintenance.models import Threshold, Measure
from maintenance.models import Variable as Var
from organization.models import Machine
from base.utils import optimize_query
from maintenance.queries.thresholds import ThresholdNode
from maintenance.queries.variables import VariableNode
from maintenance.queries.measures import MeasureNode

from cep.utils.utils import get_object_by_relay_id

class Variable(graphene.ObjectType):
    variable_id = graphene.String()
    measure_id = graphene.String()
    name = graphene.String()
    value = graphene.Float()
    datetime = graphene.DateTime()
    threshold_state = graphene.Int()
    threshold_state_str = graphene.String()

    threshold_data = graphene.Field(ThresholdNode)

class MachineNode(DjangoObjectType):
    class Meta:
        model = Machine
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    variables = graphene.List(Variable, variable_id=graphene.String(), measure_id=graphene.String())
    maintenance_status = graphene.Int()
    maintenance_warnings = graphene.Int()
    maintenance_alerts = graphene.Int()
    latest_variable_update = graphene.DateTime()

    def resolve_variables(self, info, variable_id=None, measure_id=None, **kwargs):
        variables = []
        measures = []
        if measure_id is None:
            if variable_id is None:
                measures = Measure.objects.filter(machine=self).order_by('variable', '-full_date').distinct('variable')
            else:
                measures = Measure.objects.filter(machine=self, variable=get_object_by_relay_id(Var, variable_id)).order_by('variable', '-full_date').distinct('variable')
        else:
            if variable_id is None:
                val = get_object_by_relay_id(Measure, measure_id)
                if val.machine is self:
                    measures.append(get_object_by_relay_id(Measure, measure_id))
            else:
                val = get_object_by_relay_id(Measure, measure_id)
                if val.variable is get_object_by_relay_id(Var, variable_id) and val.machine is self:
                    measures.append(get_object_by_relay_id(Measure, measure_id))
        for measure in measures:
            threshold = measure.variable.threshold_set.filter(machine=self).first()
            if threshold is not None:
                if threshold.threshold_type == Threshold.LESS_THAN:
                    if measure.value < threshold.alert:
                        t_state = 2
                        t_state_str = 'ALERT'
                    elif measure.value < threshold.warning:
                        t_state = 1
                        t_state_str = 'WARNING'
                    else:
                        t_state = 0
                        t_state_str = 'OK'
                elif threshold.threshold_type == Threshold.EQUALS:
                    if measure.value == threshold.alert:
                        t_state = 2
                        t_state_str = 'ALERT'
                    elif measure.value == threshold.warning:
                        t_state = 1
                        t_state_str = 'WARNING'
                    else:
                        t_state = 0
                        t_state_str = 'OK'
                elif threshold.threshold_type == Threshold.GREATER_THAN:
                    if measure.value > threshold.alert:
                        t_state = 2
                        t_state_str = 'ALERT'
                    elif measure.value > threshold.warning:
                        t_state = 1
                        t_state_str = 'WARNING'
                    else:
                        t_state = 0
                        t_state_str = 'OK'
                else:
                    t_state = 0
                    t_state_str = 'OK'
                variables.append(Variable(
                        variable_id=to_global_id(VariableNode._meta.name, measure.variable.pk),
                        measure_id=to_global_id(MeasureNode._meta.name, measure.pk),
                        name=measure.variable.name,
                        value=measure.value,
                        datetime=measure.full_date,
                        threshold_state=t_state,
                        threshold_state_str=t_state_str,
                        threshold_data=threshold
                    ))
            else:
                variables.append(Variable(
                        variable_id=to_global_id(VariableNode._meta.name, measure.variable.pk),
                        measure_id=to_global_id(MeasureNode._meta.name, measure.pk),
                        name=measure.variable.name,
                        value=measure.value,
                        datetime=measure.full_date,
                        threshold_state=0,
                        threshold_state_str='NO THRESHOLD'
                    ))
        return variables

    def resolve_maintenance_status(self, info, **kwargs):
        return self.maintenance_status

    def resolve_maintenance_warnings(self, info, **kwargs):
        return self.maintenance_warnings

    def resolve_maintenance_alerts(self, info, **kwargs):
        return self.maintenance_alerts

    def resolve_latest_variable_update(self, info, **kwargs):
        return self.latest_variable_update

class Query(AbstractType):
    machine = relay.Node.Field(MachineNode)

    all_machines = DjangoFilterConnectionField(
        MachineNode,
        search=graphene.String()
    )

    @login_required
    def resolve_all_machines(self, info, search=None, **kwargs):
        if search:
            return optimize_query(Machine.objects.filter(name__icontains=search), info)
        return optimize_query(Machine.objects.all(), info)