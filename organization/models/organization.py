from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model


class Organization(Model):
    """
    """
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=128)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')
        ordering = ['-pk']