from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model
from django.db import connections

class LocalBusiness(Model):
    """
    """
    organization = models.ForeignKey(
        'organization.Organization',
        on_delete=models.CASCADE,
    )
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=128)

    @property
    def maintenance_alerts(self):
        # alerts = 0
        # for machine in self.machine_set.all():
        #     if machine.maintenance_status == 2:
        #         alerts += 1
        # return alerts
        conn = connections['default']
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*) FROM (SELECT DISTINCT ON (va.variable_id)
                            va.machine_id, va.variable_id, va.id as measure_id,
                            va.value, tr.warning, tr.alert, tr.threshold_type, va.full_date FROM
                            maintenance_measure AS va
                            INNER JOIN organization_machine as ma
                            ON va.machine_id = ma.id
                            INNER JOIN maintenance_threshold as tr
                            ON va.variable_id = tr.variable_id AND ma.id = tr.machine_id
                            INNER JOIN organization_localbusiness as lb
                            ON ma.maintenance_plant_id = lb.id
                        WHERE lb.id = (%s)
                        ORDER BY va.variable_id, va.full_date DESC) as co
                    WHERE ((co.value > co.alert AND co.threshold_type = 2)
                        OR (co.value = co.alert AND co.threshold_type = 1)
                        OR (co.value < co.alert AND co.threshold_type = 0));
            """, [self.pk])
            row = cursor.fetchone()
        return row[0]

    @property
    def maintenance_warnings(self):
        # warnings = 0
        # for machine in self.machine_set.all():
        #     if machine.maintenance_status == 1:
        #         warnings += 1
        # return warnings
        conn = connections['default']
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*) FROM (SELECT DISTINCT ON (va.variable_id)
                            va.machine_id, va.variable_id, va.id as measure_id,
                            va.value, tr.warning, tr.alert, tr.threshold_type, va.full_date FROM
                            maintenance_measure AS va
                            INNER JOIN organization_machine as ma
                            ON va.machine_id = ma.id
                            INNER JOIN maintenance_threshold as tr
                            ON va.variable_id = tr.variable_id AND ma.id = tr.machine_id
                            INNER JOIN organization_localbusiness as lb
                            ON ma.maintenance_plant_id = lb.id
                        WHERE lb.id = (%s)
                        ORDER BY va.variable_id, va.full_date DESC) as co
                    WHERE ((co.value > co.warning AND co.value < co.alert AND co.threshold_type = 2)
                        OR (co.value = co.warning AND co.threshold_type = 1)
                        OR (co.value < co.warning AND co.value > co.alert AND co.threshold_type = 0));
            """, [self.pk])
            row = cursor.fetchone()
        return row[0]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Maintenance Plant')
        verbose_name_plural = _('Maintenance Plants')