from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model
from maintenance.models import Variable, Threshold, Measure
from django.db import connections

class Machine(Model):
    """
    """
    maintenance_plant = models.ForeignKey(
        'organization.LocalBusiness',
        on_delete=models.CASCADE,
    )
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=64)

    @property
    def latest_variable_update(self):
        try:
            return self.value.all().order_by('-full_date').first().full_date
        except:
            return None

    @property
    def maintenance_status(self):
        t_state = 0
        try:
            if self.maintenance_alerts > 0:
                t_state = 2
            elif self.maintenance_warnings > 0:
                t_state = 1
        except:
            pass
        return t_state

    @property
    def maintenance_alerts(self):
        # alerts = 0
        # measures = []
        # for variable in Variable.objects.filter(value__machine=self).order_by('pk').distinct('pk'):
        #     measures.append(variable.value.filter(machine=self).order_by('-full_date').first())
        # for measure in measures:
        #     threshold = self.threshold_set.filter(variable=measure.variable).first()
        #     if threshold is not None:
        #         if threshold.threshold_type == Threshold.LESS_THAN:
        #             if measure.value < threshold.alert:
        #                 alerts += 1
        #         elif threshold.threshold_type == Threshold.EQUALS:
        #             if measure.value == threshold.alert:
        #                 alerts += 1
        #         elif threshold.threshold_type == Threshold.GREATER_THAN:
        #             if measure.value > threshold.alert:
        #                 alerts += 1
        # return alerts
        conn = connections['default']
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*) FROM (SELECT DISTINCT ON (va.variable_id)
                    va.machine_id, va.variable_id, va.id as measure_id, va.value,
                    tr.warning, tr.alert, tr.threshold_type, va.full_date FROM
                    maintenance_measure AS va
                    INNER JOIN organization_machine as ma
                    ON va.machine_id = ma.id
                    INNER JOIN maintenance_threshold as tr
                    ON va.variable_id = tr.variable_id AND ma.id = tr.machine_id
                WHERE va.machine_id = (%s)
                ORDER BY va.variable_id, va.full_date DESC) as co
                WHERE ((co.value > co.alert AND co.threshold_type = 2)
                        OR (co.value = co.warning AND co.threshold_type = 1)
                        OR (co.value < co.alert AND co.threshold_type = 0));
            """, [self.pk])
            row = cursor.fetchone()
        return row[0]

    @property
    def maintenance_warnings(self):
        # warnings = 0
        # measures = []
        # for variable in Variable.objects.filter(value__machine=self).order_by('pk').distinct('pk'):
        #     measures.append(variable.value.filter(machine=self).order_by('-full_date').first())
        # for measure in measures:
        #     threshold = self.threshold_set.filter(variable=measure.variable).first()
        #     if threshold is not None:
        #         if threshold.threshold_type == Threshold.LESS_THAN:
        #             if measure.value < threshold.warning and measure.value > threshold.alert:
        #                 warnings += 1
        #         elif threshold.threshold_type == Threshold.EQUALS:
        #             if measure.value == threshold.warning:
        #                 warnings += 1
        #         elif threshold.threshold_type == Threshold.GREATER_THAN:
        #             if measure.value > threshold.warning and measure.value < threshold.alert:
        #                 warnings += 1
        # return warnings
        conn = connections['default']
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*) FROM (SELECT DISTINCT ON (va.variable_id)
                    va.machine_id, va.variable_id, va.id as measure_id, va.value, tr.warning, tr.alert,
                    tr.threshold_type, va.full_date FROM
                    maintenance_measure AS va
                    INNER JOIN organization_machine as ma
                    ON va.machine_id = ma.id
                    INNER JOIN maintenance_threshold as tr
                    ON va.variable_id = tr.variable_id AND ma.id = tr.machine_id
                WHERE va.machine_id = (%s)
                ORDER BY va.variable_id, va.full_date DESC) as co
                WHERE ((co.value > co.warning AND co.value < co.alert AND co.threshold_type = 2)
                        OR (co.value = co.warning AND co.threshold_type = 1)
                        OR (co.value < co.warning AND co.value > co.alert AND co.threshold_type = 0));
            """, [self.pk])
            row = cursor.fetchone()
        return row[0]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Machine')
        verbose_name_plural = _('Machines')
        ordering = ['-pk']