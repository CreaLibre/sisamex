from .models import LocalBusiness
from .models.machine import Machine

def allowed_local_business(func):
    def wrap(*args, **kwargs):
        result = func(*args, **kwargs)
        user = args[1].context.user

        plants = LocalBusiness.objects.none()
        for group in user.groups_ref.prefetch_related('maintenance_plants'):
            plants = plants | group.maintenance_plants.all()

        # if model_type = Model:
        #     result = result.filter(foo=bar)
        # else:
        #     result = result.filter(maintenance_plant__in=plants)

        return plants
    return wrap
