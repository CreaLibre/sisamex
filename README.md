#FIXME: Dar formato de archivo readme.

Instrucciones básicas para correr el proyecto.

1.- Construir las imagenes -> docker-compose build

2.- Construir las instancias -> docker-compose up

3.- En este momento existe varios situaciones a solucionar de forma manual, que se mencionan a continuación.
    3.1.- Entrar a la instancia "sisamex-mysql"
        3.1.1.- En la ruta raíz existe un script llamado "create_tables.sql" en donde se encuentran las tablas faltantes.
        3.1.2.- Utilizar el comando: mysql -u root -p
        3.1.3.- Proporcionar la constraseña del usuario.
        3.1.4.- Utilizar el comando: use scada;
        3.1.5.- Crear las tablas que se encuentran en el archivo mencionado.
        3.1.6.- Asigar permisos al usuario admin sobre la base de datos scada con el comando: Grant all privileges on scada.* to admin;
        3.1.7.- Salir de la base de datos con el comando: exit
        3.1.8.- Salir de la termina con el comando: exit
    3.2.- Entrar a la instancia "sisamex-postgres"
        3.2.1.- Utilizar el comando: psql -U admin postgres
        3.2.2.- Crear una base de datos faltante con el comando: create database smx_hommel;
        3.2.3.- Salir de la base de datos con el comando: exit
        3.2.4.- Salir de la termina con el comando: exit
    3.3.- Entrar a la instancia "sisamex-django-backend"
        3.3.1.- Aplicar las migraciones para la base de "sisamex-legacy": python manage.py migrate --database=sisamex-legacy
        3.3.2.- Aplicar las migraciones para la base de "cep": python manage.py migrate --database=cep
        3.3.3.- Aplicar las migraciones para la base de "default": python manage.py migrate
        3.3.4.- Crear las migraciones faltantes: python manage.py makemigrations
            3.3.4.1.- Seleccionar la opción 1)
            3.3.4.2.- Introducir el valor 1
        3.3.5.- Aplicar las migraciones para la base de "default": python manage.py migrate
        3.3.6.- Salir de la termina con el comando: exit
4.- Iniciar la instancia "sisamex-celeryworker-1"
5.- Abrir el navegador en la ruta "localhost:5555" y revisar que las tareas ejecutadas por celary no marquen errores.
6.- A este punto aún es posible que la información de la base de datos de MSSQL no haya sido cargada correctamente, para esto podemos ejecutar nuevamente la instancia "sisamex-load-data-mssql", los scripts estan válidados para no destruir la información y configuraciones que ya hayan sido creadas.
7.- Hasta al momento realizando esta secuencia de pasos ha configurado correctamente el proyecto para que pueda ser utilizado correctamente.
8.- Como paso adicional se puede crear el super usuario de django para poder acceder al administrador.
    8.1.- Entrar a la instancia "sisamex-django-backend"
    8.2.- python manage.py createsuperuser 
    8.3.- Seguir las instruciones. ;)
