-- Table: public.hommel_archivo

-- DROP TABLE public.hommel_archivo;

CREATE TABLE public.hommel_archivo
(
    id integer NOT NULL DEFAULT nextval('hommel_archivo_id_seq'::regclass),
    uuid uuid NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    prefix character varying(3) COLLATE pg_catalog."default" NOT NULL,
    number_part character varying(50) COLLATE pg_catalog."default" NOT NULL,
    operation_id integer,
    CONSTRAINT hommel_archivo_pkey PRIMARY KEY (id),
    CONSTRAINT hommel_archivo_uuid_key UNIQUE (uuid),
    CONSTRAINT hommel_archivo_operation_id_2f761ee5_fk_hommel_operation_id FOREIGN KEY (operation_id)
        REFERENCES public.hommel_operation (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.hommel_archivo
    OWNER to evient;
-- Index: hommel_archivo_operation_id_2f761ee5

-- DROP INDEX public.hommel_archivo_operation_id_2f761ee5;

CREATE INDEX hommel_archivo_operation_id_2f761ee5
    ON public.hommel_archivo USING btree
    (operation_id ASC NULLS LAST)
    TABLESPACE pg_default;