from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from maintenance.models import Measure


class MeasureForm(forms.ModelForm):
    class Meta:
        model = Measure
        fields = '__all__'
