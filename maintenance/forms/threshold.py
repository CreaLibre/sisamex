from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from maintenance.models import Threshold


class ThresholdForm(forms.ModelForm):
    class Meta:
        model = Threshold
        fields = '__all__'
