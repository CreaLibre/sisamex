from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from maintenance.models import HistoricalMeasureAlerts


class HistoricalMeasureAlertsForm(forms.ModelForm):
    class Meta:
        model = HistoricalMeasureAlerts
        fields = '__all__'