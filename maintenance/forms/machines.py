from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from maintenance.models import Machine


class MachineForm(forms.ModelForm):
    class Meta:
        model = Machine
        fields = '__all__'
