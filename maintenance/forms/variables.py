from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from maintenance.models import Variable


class VariableForm(forms.ModelForm):
    class Meta:
        model = Variable
        fields = '__all__'
