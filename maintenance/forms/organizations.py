from django import forms
from django.utils.translation import ugettext_lazy as _

from graphql import GraphQLError

from maintenance.models import Organization


class OrganizationForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = '__all__'
