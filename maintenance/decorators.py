from .models.organization import LocalBusiness
from .models.machine import Machine, Threshold, Measure
from django.db.models import Q

def allowed_local_business(func):
    def wrap(*args, **kwargs):
        result = func(*args, **kwargs)
        user = args[1].context.user

        plants = LocalBusiness.objects.none()
        for group in user.groups_ref.prefetch_related('maintenance_plants'):
            plants = plants | group.maintenance_plants.all()

        # if model_type = Model:
        #     result = result.filter(foo=bar)
        # else:
        #     result = result.filter(maintenance_plant__in=plants)

        return plants
    return wrap


def allowed_local_business_machine(func):
    def wrap(*args, **kwargs):
        result = func(*args, **kwargs)
        user = args[1].context.user

        plants = LocalBusiness.objects.none()
        machines = Machine.objects.none()
        for group in user.groups_ref.prefetch_related('maintenance_plants'):
            plants = plants | group.maintenance_plants.all()
            if (group.role == "operations_user" or group.role == "operations_supervisor" or
                group.role == "maintenance_user" or group.role == "maintenance_supervisor"):
                machines = machines | Machine.objects.filter(maintenance_plant__in=list(plants))

        # if model_type = Model:
        #     result = result.filter(foo=bar)
        # else:
        #     result = result.filter(maintenance_plant__in=plants)

        return machines
    return wrap

def allowed_local_business_threshold(func):
    def wrap(*args, **kwargs):
        result = func(*args, **kwargs)
        user = args[1].context.user

        plants = LocalBusiness.objects.none()
        thresholds = Threshold.objects.none()
        for group in user.groups_ref.prefetch_related('maintenance_plants'):
            plants = plants | group.maintenance_plants.all()
            if (group.role == "operations_user" or group.role == "operations_supervisor" or
                group.role == "maintenance_user" or group.role == "maintenance_supervisor"):
                thresholds = thresholds | Threshold.objects.filter(machine__maintenance_plant__in=list(plants))

        # if model_type = Model:
        #     result = result.filter(foo=bar)
        # else:
        #     result = result.filter(maintenance_plant__in=plants)

        return thresholds
    return wrap

def allowed_local_business_measure(func):
    def wrap(*args, **kwargs):
        result = func(*args, **kwargs)
        user = args[1].context.user

        plants = LocalBusiness.objects.none()
        measures = Measure.objects.none()
        for group in user.groups_ref.prefetch_related('maintenance_plants'):
            plants = plants | group.maintenance_plants.all()
            if (group.role == "operations_user" or group.role == "operations_supervisor" or
                group.role == "maintenance_user" or group.role == "maintenance_supervisor"):
                measures = measures | Measure.objects.filter(machine__maintenance_plant__in=list(plants))

        # if model_type = Model:
        #     result = result.filter(foo=bar)
        # else:
        #     result = result.filter(maintenance_plant__in=plants)

        return measures
    return wrap
        

