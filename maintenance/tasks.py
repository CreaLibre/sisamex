import smtplib, ssl
from datetime import datetime, timedelta

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from celery.decorators import task
from celery.utils.log import get_task_logger
from sisamex.celery import app

from django.apps import apps
from django.template import loader
from django.contrib.auth import get_user_model
from sisamex.settings import EMAIL_HOST_USER
from django.conf import settings

@app.task
def maintenance_alert_send_mail():
    Measure = apps.get_model('maintenance', 'Measure')
    for measure in Measure.objects.all():
        if measure.current_status != 0:
            threshold = measure.variable.threshold_set.filter(machine=measure.machine).first()
            subject = 'MANTENIMIENTO ALERTA {}'.format(measure.variable.name)
            message = ('El valor de la variable {} a superado el limite de {} con un valor de {}.'\
                .format(measure.variable.name, threshold.alert, measure.value))
            email_from = settings.DEFAULT_FROM_EMAIL

            recipient_list = []
            supervisors = get_user_model().objects.filter(groups_ref__role__in=['maintenance_supervisor'])
            mime_to_string = supervisors[0].email
            recipient_list.append(supervisors[0])
            for supervisor in supervisors[1:]:
                recipient_list.append(supervisor.email)
                mime_to_string = mime_to_string + ',' + supervisor.email
            msg = MIMEMultipart("alternative")
            msg["Subject"] = subject
            msg["From"] = email_from
            msg["To"] = mime_to_string
            template = loader.get_template('email_template.html')
            html_content = template.render({
                'subject': subject,
                'text_content': message
            })
            part1 = MIMEText(message, "plain")
            part2 = MIMEText(html_content, "html")
            msg.attach(part1)
            msg.attach(part2)
            context = ssl.create_default_context()
            with smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT) as server:
                server.starttls(context=context)
                server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
                server.sendmail(
                    email_from, recipient_list, msg.as_string()
                )