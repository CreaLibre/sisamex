import graphene
from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from maintenance.models import Measure
from base.utils import optimize_query
from maintenance.decorators import allowed_local_business_measure

class MeasureNode(DjangoObjectType):
    class Meta:
        model = Measure
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    current_status = graphene.Int()

    def resolve_current_status(self, info, **kwargs):
        return self.current_status


class Query(AbstractType):
    measure = relay.Node.Field(MeasureNode)

    all_measures = DjangoFilterConnectionField(
        MeasureNode,
    )

    @login_required
    @allowed_local_business_measure
    def resolve_all_measures(self, info, **kwargs):
        return optimize_query(Measure.objects.all(), info)