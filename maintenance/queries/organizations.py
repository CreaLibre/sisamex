from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField

from maintenance.models import Organization
from base.utils import optimize_query

class MaintenanceOrganizationNode(DjangoObjectType):
    class Meta:
        model = Organization
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    maintenance_organization = relay.Node.Field(MaintenanceOrganizationNode)

    all_maintenance_organizations = DjangoFilterConnectionField(
        MaintenanceOrganizationNode,
    )

    def resolve_all_maintenance_organizations(self, info, **kwargs):
        return optimize_query(Organization.objects.all(), info)
