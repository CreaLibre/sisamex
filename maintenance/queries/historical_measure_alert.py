from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField

from maintenance.models import HistoricalMeasureAlerts
from base.utils import optimize_query

class HistoricalMeasureAlertsNode(DjangoObjectType):
    class Meta:
        model = HistoricalMeasureAlerts
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    HistoricalMeasureAlerts = relay.Node.Field(HistoricalMeasureAlertsNode)

    all_historical_measure_alerts = DjangoFilterConnectionField(
        HistoricalMeasureAlertsNode,
    )

    def resolve_all_historical_measure_alerts(self, info, **kwargs):
        return optimize_query(HistoricalMeasureAlerts.objects.all(), info)