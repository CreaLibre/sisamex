import graphene
from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from maintenance.models import Threshold
from base.utils import optimize_query
from maintenance.decorators import allowed_local_business_threshold

class ThresholdNode(DjangoObjectType):
    class Meta:
        model = Threshold
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    threshold_type_str = graphene.String()

    def resolve_threshold_type_str(self, info, **kwargs):
        if self.threshold_type == self.LESS_THAN:
            return "<"
        elif self.threshold_type == self.EQUALS:
            return "="
        elif self.threshold_type == self.GREATER_THAN:
            return ">"
        else:
            return "?"



class Query(AbstractType):
    threshold = relay.Node.Field(ThresholdNode)

    all_thresholds = DjangoFilterConnectionField(
        ThresholdNode,
    )

    @login_required
    @allowed_local_business_threshold
    def resolve_all_thresholds(self, info, **kwargs):
        return optimize_query(Threshold.objects.all(), info)