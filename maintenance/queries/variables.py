from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField

from maintenance.models import Variable
from base.utils import optimize_query

class VariableNode(DjangoObjectType):
    class Meta:
        model = Variable
        interfaces = (relay.Node,)
        filter_fields = '__all__'


class Query(AbstractType):
    variable = relay.Node.Field(VariableNode)

    all_variables = DjangoFilterConnectionField(
        VariableNode,
    )

    def resolve_all_variables(self, info, **kwargs):
        return optimize_query(Variable.objects.all(), info)