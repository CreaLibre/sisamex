import graphene
from graphene import relay, AbstractType
from graphql_relay import to_global_id

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from maintenance.models import Machine, Threshold
from maintenance.models import Variable as Var
from base.utils import optimize_query
from maintenance.queries.thresholds import ThresholdNode

from maintenance.decorators import allowed_local_business_machine
from maintenance.queries.variables import VariableNode
from maintenance.queries.measures import MeasureNode

class MaintenanceVariable(graphene.ObjectType):
    variable_id = graphene.String()
    measure_id = graphene.String()
    name = graphene.String()
    value = graphene.Float()
    datetime = graphene.DateTime()
    threshold_state = graphene.Int()
    threshold_state_str = graphene.String()

    threshold_data = graphene.Field(ThresholdNode)

class MaintenanceMachineNode(DjangoObjectType):
    class Meta:
        model = Machine
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    variables = graphene.List(MaintenanceVariable)
    maintenance_status = graphene.Int()
    maintenance_warnings = graphene.Int()
    maintenance_alerts = graphene.Int()

    def resolve_variables(self, info, **kwargs):
        variables = []
        measures = []
        for variable in Var.objects.filter(value__machine=self.pk).order_by('pk').distinct('pk'):
            measures.append(variable.value.filter(machine=self.pk).order_by('-full_date').first())
        for measure in measures:
            threshold = self.threshold_set.filter(variable=measure.variable).first()
            if threshold is not None:
                if threshold.threshold_type == Threshold.LESS_THAN:
                    if measure.value < threshold.alert:
                        t_state = 2
                        t_state_str = 'ALERT'
                    elif measure.value < threshold.warning:
                        t_state = 1
                        t_state_str = 'WARNING'
                    else:
                        t_state = 0
                        t_state_str = 'OK'
                elif threshold.threshold_type == Threshold.EQUALS:
                    if measure.value == threshold.alert:
                        t_state = 2
                        t_state_str = 'ALERT'
                    elif measure.value == threshold.warning:
                        t_state = 1
                        t_state_str = 'WARNING'
                    else:
                        t_state = 0
                        t_state_str = 'OK'
                elif threshold.threshold_type == Threshold.GREATER_THAN:
                    if measure.value > threshold.alert:
                        t_state = 2
                        t_state_str = 'ALERT'
                    elif measure.value > threshold.warning:
                        t_state = 1
                        t_state_str = 'WARNING'
                    else:
                        t_state = 0
                        t_state_str = 'OK'
                else:
                    t_state = 0
                    t_state_str = 'OK'
                variables.append(MaintenanceVariable(
                        variable_id=to_global_id(VariableNode._meta.name, measure.variable.pk),
                        measure_id=to_global_id(MeasureNode._meta.name, measure.pk),
                        name=measure.variable.name,
                        value=measure.value,
                        datetime=measure.full_date,
                        threshold_state=t_state,
                        threshold_state_str=t_state_str,
                        threshold_data=threshold
                    ))
            else:
                variables.append(MaintenanceVariable(
                        variable_id=to_global_id(VariableNode._meta.name, measure.variable.pk),
                        measure_id=to_global_id(MeasureNode._meta.name, measure.pk),
                        name=measure.variable.name,
                        value=measure.value,
                        datetime=measure.full_date,
                        threshold_state=0,
                        threshold_state_str='NO THRESHOLD'
                    ))
        return variables

    def resolve_maintenance_status(self, info, **kwargs):
        return self.maintenance_status

    def resolve_maintenance_warnings(self, info, **kwargs):
        return self.maintenance_warnings

    def resolve_maintenance_alerts(self, info, **kwargs):
        return self.maintenance_alerts

class Query(AbstractType):
    maintenance_machine = relay.Node.Field(MaintenanceMachineNode)

    all_maintenance_machines = DjangoFilterConnectionField(
        MaintenanceMachineNode,
    )

    @login_required
    @allowed_local_business_machine
    def resolve_all_maintenance_machines(self, info, **kwargs):
        return optimize_query(Machine.objects.all(), info)