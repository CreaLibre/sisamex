import graphene
from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from maintenance.models import LocalBusiness
from base.utils import optimize_query

from ..decorators import allowed_local_business

class MaintenanceLocalBusinessNode(DjangoObjectType):
    class Meta:
        model = LocalBusiness
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    maintenance_warnings = graphene.Int()
    maintenance_alerts = graphene.Int()

    def resolve_maintenance_warnings(self, info, **kwargs):
        return self.maintenance_warnings

    def resolve_maintenance_alerts(self, info, **kwargs):
        return self.maintenance_alerts


class Query(AbstractType):
    maintenance_local_business = relay.Node.Field(MaintenanceLocalBusinessNode)

    all_maintenance_local_business = DjangoFilterConnectionField(
        MaintenanceLocalBusinessNode,
    )

    @login_required
    #@allowed_local_business
    def resolve_all_maintenance_local_business(self, info, **kwargs):
        return optimize_query(LocalBusiness.objects.all(), info)
