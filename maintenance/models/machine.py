from datetime import datetime
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.template import loader
from django.conf import settings

from base.models import Model

from .machine_historical import HistoricalMeasureAlerts
from django.db import connections


class Machine(Model):
    """
    """
    maintenance_plant = models.ForeignKey(
        'maintenance.LocalBusiness',
        on_delete=models.CASCADE,
    )
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=64)

    @property
    def maintenance_status(self):
        t_state = 0
    # for measure in self.value.all():
    #     threshold = self.threshold_set.filter(variable=measure.variable).first()
    #     if threshold is not None:
    #         if threshold.threshold_type == Threshold.LESS_THAN:
    #             if measure.value < threshold.alert:
    #                 t_state = 2
    #             elif measure.value < threshold.warning and t_state < 2:
    #                 t_state = 1
    #         elif threshold.threshold_type == Threshold.EQUALS:
    #             if measure.value == threshold.alert:
    #                 t_state = 2
    #             elif measure.value == threshold.warning and t_state < 2:
    #                 t_state = 1
    #         elif threshold.threshold_type == Threshold.GREATER_THAN:
    #             if measure.value > threshold.alert:
    #                 t_state = 2
    #             elif measure.value > threshold.warning and t_state < 2:
    #                 t_state = 1
        try:
            if self.maintenance_alerts > 0:
                t_state = 2
            elif self.maintenance_warnings > 0:
                t_state = 1
        except:
            pass
        return t_state

    @property
    def maintenance_alerts(self):
        # alerts = 0
        # measures = []
        # for variable in Variable.objects.filter(value__machine=self).order_by('pk').distinct('pk'):
        #     measures.append(variable.value.filter(machine=self).order_by('-full_date').first())
        # for measure in measures:
        #     if measure.current_status == 2:
        ##         alerts += 1
        # return alerts
        conn = connections['default']
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*) FROM (
                    SELECT MAX(va.id) FROM
                        maintenance_measure AS va
                        INNER JOIN maintenance_machine as ma
                        ON va.machine_id = ma.id
                        INNER JOIN maintenance_threshold as tr
                        ON ma.id = tr.machine_id
                        INNER JOIN (SELECT variable_id, MAX(full_date) as full_date FROM maintenance_measure
		                            GROUP BY variable_id) as fd
                        ON fd.variable_id = va.variable_id
                    WHERE ma.id = (%s) AND va.full_date = va.full_date
	                    AND ((va.value > tr.alert AND tr.threshold_type = 2)
		                    OR (va.value = tr.warning AND tr.threshold_type = 1)
		                    OR (va.value < tr.alert AND tr.threshold_type = 0))
                    GROUP BY va.variable_id) as co;
            """, [self.pk])
            row = cursor.fetchone()
        return row[0]

    @property
    def maintenance_warnings(self):
        # warnings = 0
        # measures = []
        # for variable in Variable.objects.filter(value__machine=self).order_by('pk').distinct('pk'):
        #     measures.append(variable.value.filter(machine=self).order_by('-full_date').first())
        # for measure in measures:
        #     if measure.current_status == 1:
        #         warnings += 1
        # return warnings
        conn = connections['default']
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*) FROM (
                    SELECT MAX(va.id) FROM
                        maintenance_measure AS va
                        INNER JOIN maintenance_machine as ma
                        ON va.machine_id = ma.id
                        INNER JOIN maintenance_threshold as tr
                        ON ma.id = tr.machine_id
                        INNER JOIN (SELECT variable_id, MAX(full_date) as full_date FROM maintenance_measure
		                            GROUP BY variable_id) as fd
                        ON fd.variable_id = va.variable_id
                    WHERE ma.id = (%s) AND va.full_date = va.full_date
	                    AND ((va.value > tr.warning AND va.value < tr.alert AND tr.threshold_type = 2)
		                    OR (va.value = tr.warning AND tr.threshold_type = 1)
		                    OR (va.value < tr.warning AND va.value > tr.alert AND tr.threshold_type = 0))
                    GROUP BY va.variable_id) as co;
            """, [self.pk])
            row = cursor.fetchone()
        return row[0]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Machine')
        verbose_name_plural = _('Machines')
        ordering = ['-pk']
        #db_table = 'sisbox_maquinas'


class Variable(Model):
    """
    """
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=128)
    unit = models.CharField(_('Unit'), max_length=128, null=True, blank=True)
#    machine = models.ForeignKey(
#        'Machine',
#        on_delete=models.CASCADE,
#    )

    def __str__(self):
        return f"{self.name} {self.unit}"


    class Meta:
        verbose_name = _('Variable')
        verbose_name_plural = _('Variables')


    """ class Unit(Model):
    """
    """
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=64)
    symbol= models.CharField(_('Symbol'), blank=False, unique=True, max_length=8)


    def __str__(self):
        return f"{self.name} ({self.symbol})"


    """


class Threshold(Model):
    """
    """

    LESS_THAN = 0
    EQUALS = 1
    GREATER_THAN = 2

    THRESHOLD_TYPES = (
        (LESS_THAN, '<'),
        (EQUALS, '='),
        (GREATER_THAN, '>'),
    )

    machine = models.ForeignKey(
        'organization.Machine',
        on_delete=models.CASCADE,
    )
    warning = models.IntegerField(_('Warning Level'), blank=True, null=True)
    alert = models.IntegerField(_('Alert Level'))
    threshold_type = models.IntegerField(_('Type'), choices=THRESHOLD_TYPES, default=GREATER_THAN)
    variable = models.ForeignKey(
        'Variable',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.machine} : {self.variable} ({self.alert})"

    def clean(self):
        super().clean()
        if self.threshold_type == self.LESS_THAN:
            if self.alert > self.warning:
                raise ValidationError('A "<" type threshold can have its alert be greater than its warning')
        elif self.threshold_type == self.GREATER_THAN:
            if self.alert < self.warning:
                raise ValidationError('A ">" type threshold can have its alert be less than its warning')
        elif self.threshold_type == self.EQUALS:
            if self.alert != self.warning:
                raise ValidationError('A "=" type threshold can have its alert be different than its warning')

    class Meta:
        verbose_name = _('Threshold')
        verbose_name_plural = _('Thresholds')



class Measure(Model):
    machine = models.ForeignKey(
        'organization.Machine',
        on_delete=models.CASCADE,
        verbose_name=_('Machine'),
        related_name='value',
    )
    variable = models.ForeignKey(
        'maintenance.Variable',
        on_delete=models.CASCADE,
        verbose_name=_('Variable'),
        related_name='value'
    )
    value = models.FloatField(_('Value'))
    full_date = models.DateTimeField(_('Date and hour'))

    @property
    def current_status(self):
        threshold = self.variable.threshold_set.filter(machine=self.machine).first()
        if threshold is not None:
            try:#??????
                if threshold.threshold_type == Threshold.LESS_THAN:
                    if self.value < threshold.alert:
                        return 2
                    elif self.value < threshold.warning:
                        return 1
                    else:
                        return 0
                elif threshold.threshold_type == Threshold.EQUALS:
                    if self.value == threshold.alert:
                        return 2
                    elif self.value == threshold.warning:
                        return 1
                    else:
                        return 0
                elif threshold.threshold_type == Threshold.GREATER_THAN:
                    if self.value > threshold.alert:
                        return 2
                    elif self.value > threshold.warning:
                        return 1
                    else:
                        return 0
                else:
                    return 0
            except:
                return 0
        else:
            return 0

    def save(self, *args, **kwargs):
        instance = super().save(*args, **kwargs)
        try:
            if self.current_status != 0:
                HistoricalMeasureAlerts.objects.create(
                    status=self.current_status,
                    full_date=datetime.now(),
                    measure=self,
                    machine=self.machine,
                    variable=self.variable
                ).save()
                threshold = self.variable.threshold_set.filter(machine=self.machine).first()
                subject = 'MANTENIMIENTO ALERTA {}'.format(self.variable.name)
                message = ('El valor de la variable {} a superado el limite de {} con un valor de {}.'\
                    .format(self.variable.name, threshold.alert, self.value))
                email_from = settings.DEFAULT_FROM_EMAIL

                recipient_list = []
                supervisors = get_user_model().objects.filter(groups__role__in=['maintenance_supervisor'])
                mime_to_string = supervisors[0].email
                recipient_list.append(supervisors[0])
                for supervisor in supervisors[1:]:
                    recipient_list.append(supervisor.email)
                    mime_to_string = mime_to_string + ',' + supervisor.email
                msg = MIMEMultipart("alternative")
                msg["Subject"] = subject
                msg["From"] = email_from
                msg["To"] = mime_to_string
                template = loader.get_template('email_template.html')
                html_content = template.render({
                    'subject': subject,
                    'text_content': message
                })
                part1 = MIMEText(message, "plain")
                part2 = MIMEText(html_content, "html")
                msg.attach(part1)
                msg.attach(part2)
                context = ssl.create_default_context()
                with smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT) as server:
                    server.starttls(context=context)
                    server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
                    server.sendmail(
                        email_from, recipient_list, msg.as_string()
                    )
        except:
            pass
        return instance

    def __str__(self):
        return f'{self.machine}-{self.variable}: {self.value} {self.full_date}'
