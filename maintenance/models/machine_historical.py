from django.db import models
from django.utils.timezone import localtime
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model

class HistoricalMeasureAlerts(Model):
    OK = 0
    WARNING = 1
    ALERT = 2

    MEASURE_STATUS = (
        (OK, 'OK'),
        (WARNING, 'WARNING'),
        (ALERT, 'ALERT'),
    )

    status = models.IntegerField(_('Status'), choices=MEASURE_STATUS, default=OK)
    full_date = models.DateTimeField(_('Date and hour'))

    machine = models.ForeignKey(
        'organization.Machine',
        on_delete=models.CASCADE,
        verbose_name=_('Machine'),
        related_name='historical_alerts'
    )
    measure = models.ForeignKey(
        'maintenance.Measure',
        on_delete=models.CASCADE,
        verbose_name=_('Measure'),
        related_name='historical_alerts'
    )
    variable = models.ForeignKey(
        'maintenance.Variable',
        on_delete=models.CASCADE,
        verbose_name=_('Variable'),
        related_name='historical_alerts'
    )

    @property
    def status_str(self):
        if self.status == self.OK:
            return 'OK'
        elif self.status == self.WARNING:
            return 'WARNING'
        elif self.status == self.ALERT:
            return 'ALERT'
        else:
            return '?'

    def __str__(self):
        return f'{self.measure} - Status: {self.status_str} on {localtime(self.full_date).strftime("%d/%m/%Y %H:%M:%S")}'

    class Meta:
        verbose_name = _('Historical Alert')
        verbose_name_plural = _('Historical Alerts')
        ordering = ['-full_date']