from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db import models

from base.models import Model

from django.db import connections


class Organization(Model):
    """
    """
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=128)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')
        ordering = ['-pk']


class LocalBusiness(Model):
    """
    """
    organization = models.ForeignKey(
        'Organization',
        on_delete=models.CASCADE,
    )
    name = models.CharField(_('Name'), blank=False, unique=True, max_length=128)

    @property
    def maintenance_alerts(self):
        # alerts = 0
        # for machine in self.machine_set.all():
        #     if machine.maintenance_status == 2:
        #         alerts += 1
        # return alerts
        conn = connections['default']
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*) FROM (
                    SELECT MAX(va.id) FROM
                        maintenance_measure AS va
                        INNER JOIN maintenance_machine as ma
                        ON va.machine_id = ma.id
                        INNER JOIN maintenance_threshold as tr
                        ON ma.id = tr.machine_id
                        INNER JOIN maintenance_localbusiness as lb
                        ON ma.maintenance_plant_id = lb.id
                        INNER JOIN (SELECT variable_id, MAX(full_date) as full_date FROM maintenance_measure
                                    GROUP BY variable_id) as fd
                        ON fd.variable_id = va.variable_id
                    WHERE lb.id = (%s) AND va.full_date = va.full_date
                        AND ((va.value > tr.alert AND tr.threshold_type = 2)
                            OR (va.value = tr.alert AND tr.threshold_type = 1)
                            OR (va.value < tr.alert AND tr.threshold_type = 0))
                    GROUP BY va.variable_id) as co;
            """, [self.pk])
            row = cursor.fetchone()
        return row[0]

    @property
    def maintenance_warnings(self):
        # warnings = 0
        # for machine in self.machine_set.all():
        #     if machine.maintenance_status == 1:
        #         warnings += 1
        # return warnings
        conn = connections['default']
        with conn.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*) FROM (
                    SELECT MAX(va.id) FROM
                        maintenance_measure AS va
                        INNER JOIN maintenance_machine as ma
                        ON va.machine_id = ma.id
                        INNER JOIN maintenance_threshold as tr
                        ON ma.id = tr.machine_id
                        INNER JOIN maintenance_localbusiness as lb
                        ON ma.maintenance_plant_id = lb.id
                        INNER JOIN (SELECT variable_id, MAX(full_date) as full_date FROM maintenance_measure
                                    GROUP BY variable_id) as fd
                        ON fd.variable_id = va.variable_id
                    WHERE lb.id = (%s) AND va.full_date = va.full_date
                        AND ((va.value > tr.warning AND va.value < tr.alert AND tr.threshold_type = 2)
		                    OR (va.value = tr.warning AND tr.threshold_type = 1)
		                    OR (va.value < tr.warning AND va.value > tr.alert AND tr.threshold_type = 0))
                    GROUP BY va.variable_id) as co;
            """, [self.pk])
            row = cursor.fetchone()
        return row[0]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Maintenance Plant')
        verbose_name_plural = _('Maintenance Plants')
