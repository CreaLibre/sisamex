import graphene

from .queries.measures import Query as measure_query
from .queries.machines import Query as machines_query
from .queries.variables import Query as variables_query
from .queries.thresholds import Query as threshold_query
from .queries.organizations import Query as organizations_query
from .queries.local_business import Query as local_business_query
from .queries.historical_measure_alert import Query as historical_measure_alert_query

from .mutations.measures import Mutation as measure_mutation
from .mutations.machines import Mutation as machines_mutation
from .mutations.variables import Mutation as variables_mutation
from .mutations.thresholds import Mutation as threshold_mutation
from .mutations.organizations import Mutation as organizations_mutation
from .mutations.local_business import Mutation as local_business_mutation
from .mutations.historical_measure_alert import Mutation as historical_measure_alert_mutation


class Query(
    measure_query,
    machines_query,
    variables_query,
    threshold_query,
    organizations_query,
    local_business_query,
    historical_measure_alert_query,
    graphene.AbstractType
):
    pass


class Mutation(
    measure_mutation,
    machines_mutation,
    variables_mutation,
    threshold_mutation,
    organizations_mutation,
    historical_measure_alert_mutation,
    graphene.AbstractType,
):
    pass