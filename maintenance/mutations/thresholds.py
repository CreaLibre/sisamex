import graphene
from graphene_django.forms.types import ErrorType

from maintenance.models import Threshold
from maintenance.queries.thresholds import ThresholdNode
from maintenance.forms import ThresholdForm
from base.graphene.mutations import GenericMutations


class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    threshold = graphene.Field(ThresholdNode)

    class Meta:
        generic_name = 'threshold'
        form_class = ThresholdForm
        model = Threshold

    @classmethod
    def perform_create(cls, create_cls, form, info):
        threshold = form.save()
        return create_cls(errors=[], threshold=threshold)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        threshold = form.save()
        return update_cls(errors=[], threshold=threshold)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        threshold = instance.delete()
        return delete_cls(errors=[], threshold=threshold)
