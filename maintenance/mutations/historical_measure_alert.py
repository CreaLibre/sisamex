import graphene
from graphene_django.forms.types import ErrorType

from maintenance.models import HistoricalMeasureAlerts
from maintenance.queries.historical_measure_alert import HistoricalMeasureAlertsNode
from maintenance.forms import HistoricalMeasureAlertsForm
from base.graphene.mutations import GenericMutations


class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    historical_measure_alert = graphene.Field(HistoricalMeasureAlertsNode)

    class Meta:
        generic_name = 'historical_measure_alert'
        form_class = HistoricalMeasureAlertsForm
        model = HistoricalMeasureAlerts

    @classmethod
    def perform_create(cls, create_cls, form, info):
        historical_measure_alert = form.save()
        return create_cls(errors=[], historical_measure_alert=historical_measure_alert)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        historical_measure_alert = form.save()
        return update_cls(errors=[], historical_measure_alert=historical_measure_alert)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        historical_measure_alert = instance.delete()
        return delete_cls(errors=[], historical_measure_alert=historical_measure_alert)