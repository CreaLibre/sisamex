import graphene
from graphene_django.forms.types import ErrorType

from maintenance.models import LocalBusiness
from maintenance.queries.local_business import MaintenanceLocalBusinessNode
from maintenance.forms import LocalBusinessForm
from base.graphene.mutations import GenericMutations


class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    local_business = graphene.Field(MaintenanceLocalBusinessNode)

    class Meta:
        generic_name = 'local_business'
        form_class = LocalBusinessForm
        model = LocalBusiness

    @classmethod
    def perform_create(cls, create_cls, form, info):
        local_business = form.save()
        return create_cls(errors=[], local_business=local_business)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        local_business = form.save()
        return update_cls(errors=[], local_business=local_business)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        local_business = instance.delete()
        return delete_cls(errors=[], local_business=local_business)
