import graphene
from graphene_django.forms.types import ErrorType

from maintenance.models import Variable
from maintenance.queries.variables import VariableNode
from maintenance.forms import VariableForm
from base.graphene.mutations import GenericMutations


class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    variable = graphene.Field(VariableNode)

    class Meta:
        generic_name = 'variable'
        form_class = VariableForm
        model = Variable

    @classmethod
    def perform_create(cls, create_cls, form, info):
        variable = form.save()
        return create_cls(errors=[], variable=variable)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        variable = form.save()
        return update_cls(errors=[], variable=variable)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        variable = instance.delete()
        return delete_cls(errors=[], variable=variable)
