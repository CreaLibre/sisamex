import graphene
from graphene_django.forms.types import ErrorType

from maintenance.models import Measure
from maintenance.queries.measures import MeasureNode
from maintenance.forms import MeasureForm
from base.graphene.mutations import GenericMutations


class Mutation(GenericMutations):
    errors = graphene.List(ErrorType)
    measure = graphene.Field(MeasureNode)

    class Meta:
        generic_name = 'measure'
        form_class = MeasureForm
        model = Measure

    @classmethod
    def perform_create(cls, create_cls, form, info):
        measure = form.save()
        return create_cls(errors=[], measure=measure)

    @classmethod
    def perform_update(cls, update_cls, form, info):
        measure = form.save()
        return update_cls(errors=[], measure=measure)

    @classmethod
    def perform_delete(cls, delete_cls, instance, info):
        measure = instance.delete()
        return delete_cls(errors=[], measure=measure)
