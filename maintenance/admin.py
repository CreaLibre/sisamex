from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from maintenance.models import(
    Machine,
    Threshold,
    Organization,
    LocalBusiness,
    Variable,
    Measure,
    HistoricalMeasureAlerts,
)

@admin.register(Measure)
class MeasureAdmin(ImportExportModelAdmin):
    list_filter = ['machine']

# @admin.register(Machine)
# class MachineAdmin(ImportExportModelAdmin):
#     list_display = ['name', 'maintenance_plant', 'maintenance_status', 'maintenance_warnings', 'maintenance_alerts']
#     readonly_fields = ('maintenance_status', 'maintenance_warnings', 'maintenance_alerts',)

@admin.register(Threshold)
class ThresholdAdmin(ImportExportModelAdmin):
    list_filter = ['machine']

@admin.register(Variable)
class VariableAdmin(ImportExportModelAdmin):
    pass

# @admin.register(Organization)
# class OrganizationAdmin(ImportExportModelAdmin):
#     pass

# @admin.register(LocalBusiness)
# class LocalBusinessAdmin(ImportExportModelAdmin):
#     list_display = ['name', 'organization', 'maintenance_warnings', 'maintenance_alerts']
#     readonly_fields = ('maintenance_warnings', 'maintenance_alerts',)

@admin.register(HistoricalMeasureAlerts)
class HistoricalMeasureAlertsAdmin(ImportExportModelAdmin):
    readonly_fields = ('status', 'full_date', 'variable', 'measure', 'machine')
    list_filter = ['machine']

