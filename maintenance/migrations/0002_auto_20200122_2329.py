# Generated by Django 3.0.2 on 2020-01-22 23:29

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('maintenance', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocalBusiness',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('name', models.CharField(max_length=128, unique=True, verbose_name='Name')),
            ],
            options={
                'verbose_name': 'Maintenance Plant',
                'verbose_name_plural': 'Maintenance Plants',
            },
        ),
        migrations.CreateModel(
            name='Machine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Name')),
                ('maintenance_plant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maintenance.LocalBusiness')),
            ],
            options={
                'verbose_name': 'Machine',
                'verbose_name_plural': 'Machines',
                'ordering': ['-pk'],
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('name', models.CharField(max_length=128, unique=True, verbose_name='Name')),
            ],
            options={
                'verbose_name': 'Organization',
                'verbose_name_plural': 'Organizations',
                'ordering': ['-pk'],
            },
        ),
        migrations.CreateModel(
            name='Threshold',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('warning', models.IntegerField(verbose_name='Warning Level')),
                ('maximum', models.IntegerField(verbose_name='Maximum Level')),
                ('machine', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maintenance.Machine')),
            ],
            options={
                'verbose_name': 'Unit of Measurement',
                'verbose_name_plural': 'Unit of Measurements',
            },
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Name')),
                ('symbol', models.CharField(max_length=8, unique=True, verbose_name='Symbol')),
            ],
            options={
                'verbose_name': 'Unit of Measurement',
                'verbose_name_plural': 'Unit of Measurements',
            },
        ),
        migrations.CreateModel(
            name='Variable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('name', models.CharField(max_length=128, unique=True, verbose_name='Name')),
                ('machine', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maintenance.Machine')),
                ('unit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maintenance.Unit')),
            ],
            options={
                'verbose_name': 'Variable',
                'verbose_name_plural': 'Variables',
            },
        ),
        migrations.DeleteModel(
            name='Maintenance',
        ),
        migrations.AddField(
            model_name='threshold',
            name='variable',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maintenance.Variable'),
        ),
        migrations.AddField(
            model_name='localbusiness',
            name='organization',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='maintenance.Organization'),
        ),
    ]
