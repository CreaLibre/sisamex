from maintenance.models import Threshold

def on_threshold_save(sender, instance, **kwargs):
    if instance.threshold_type == instance.LESS_THAN:
        if instance.alert > instance.warning:
            instance.alert = instance.warning
            instance.save()
    elif instance.threshold_type == instance.GREATER_THAN:
        if instance.alert < instance.warning:
            instance.alert = instance.warning
            instance.save()
    elif instance.threshold_type == instance.EQUALS:
        if instance.alert != instance.warning:
            instance.alert = instance.warning
            instance.save()