from django.apps import AppConfig
from django.db.models.signals import post_save


class MaintenanceConfig(AppConfig):
    name = 'maintenance'

    def ready(self):
        from maintenance.models import Threshold
        from maintenance.signals import on_threshold_save

        post_save.connect(on_threshold_save, sender=Threshold)
