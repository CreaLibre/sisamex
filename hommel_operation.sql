-- Table: public.hommel_operation

-- DROP TABLE public.hommel_operation;

CREATE TABLE public.hommel_operation
(
    id integer NOT NULL DEFAULT nextval('hommel_operation_id_seq'::regclass),
    uuid uuid NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name character varying(6) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT hommel_operation_pkey PRIMARY KEY (id),
    CONSTRAINT hommel_operation_uuid_key UNIQUE (uuid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.hommel_operation
    OWNER to evient;