-- Table: public.hommel_mapa_campos

-- DROP TABLE public.hommel_mapa_campos;

CREATE TABLE public.hommel_mapa_campos
(
    id integer NOT NULL DEFAULT nextval('hommel_mapa_campos_id_seq'::regclass),
    uuid uuid NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    field_name character varying(150) COLLATE pg_catalog."default" NOT NULL,
    "column" integer,
    creation_date timestamp with time zone NOT NULL,
    validate boolean NOT NULL,
    map_id integer,
    CONSTRAINT hommel_mapa_campos_pkey PRIMARY KEY (id),
    CONSTRAINT hommel_mapa_campos_uuid_key UNIQUE (uuid),
    CONSTRAINT hommel_mapa_campos_map_id_dea9ac8d_fk_hommel_mapa_id FOREIGN KEY (map_id)
        REFERENCES public.hommel_mapa (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.hommel_mapa_campos
    OWNER to evient;
-- Index: hommel_mapa_campos_map_id_dea9ac8d

-- DROP INDEX public.hommel_mapa_campos_map_id_dea9ac8d;

CREATE INDEX hommel_mapa_campos_map_id_dea9ac8d
    ON public.hommel_mapa_campos USING btree
    (map_id ASC NULLS LAST)
    TABLESPACE pg_default;