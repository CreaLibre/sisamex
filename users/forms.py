from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from users.models import User, Group
from django import forms
from django.core.exceptions import ValidationError

class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'is_superuser', 'is_staff', 'is_active', 'user_permissions')

    group = forms.ModelChoiceField(queryset=Group.objects.all(), empty_label='SELECT GROUP')

    # def clean(self):
    #     self.cleaned_data['groups_ref'] = [self.cleaned_data['group']]
    #     return cleaned_data

    def save(self, commit=True):
        instance = super(CustomUserCreationForm, self).save(commit=False)
        instance.groups_ref.clear()
        instance.groups_ref.add(self.cleaned_data['group'])
        if commit:
            instance.save()
        return instance


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'is_superuser', 'is_staff', 'is_active', 'user_permissions')

    group = forms.ModelChoiceField(queryset=Group.objects.all(), empty_label='SELECT GROUP')

    def __init__(self, *args, **kwargs):
        super(CustomUserChangeForm, self).__init__(*args, **kwargs)
        try:
            self.fields['group'].initial = self.instance.groups_ref.first()
        except:
            pass

    def save(self, commit=True):
        instance = super(CustomUserChangeForm, self).save(commit=False)
        instance.groups_ref.clear()
        instance.groups_ref.add(self.cleaned_data['group'])
        if commit:
            instance.save()
        return instance
    

class GroupCustomForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['role', 'users', 'maintenance_plants']

    def clean(self):
        users = [x for x in self.cleaned_data['users'] if x not in self.instance.users.all()]
        try:
            user = users[0]
        except:
            return
        if user.groups_ref.exclude(pk=self.instance.pk):
            raise ValidationError("Un usuario ya se encuentra en otro grupo")