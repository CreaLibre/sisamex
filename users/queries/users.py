import graphene
from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from users.models import User
from base.utils import optimize_query
from .groups import GroupNode

class UserNode(DjangoObjectType):
    class Meta:
        model = User
        interfaces = (relay.Node,)
        filter_fields = '__all__'

    groups = DjangoFilterConnectionField(GroupNode)

    def resolve_groups(self, info, **kwargs):
        return optimize_query(self.groups.all(), info)    


class Query(AbstractType):
    user = relay.Node.Field(UserNode)

    current_user = DjangoFilterConnectionField(
        UserNode,
    )

    @login_required
    def resolve_current_user(self, info, **kwargs):
        return optimize_query(User.objects.filter(pk=info.context.user.pk), info)