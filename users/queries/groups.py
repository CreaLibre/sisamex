import graphene
from graphene import relay, AbstractType

from graphene_django.types import DjangoObjectType
from graphene_django.filter import  DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from users.models import Group
from base.utils import optimize_query

class GroupNode(DjangoObjectType):
    class Meta:
        model = Group
        interfaces = (relay.Node,)
        filter_fields = '__all__'