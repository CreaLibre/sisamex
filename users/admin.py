from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth.admin import UserAdmin
# from django.contrib.auth.forms import UserCreationForm, UserChangeForm,
# AdminPasswordChangeForm
from users.forms import CustomUserCreationForm, CustomUserChangeForm, GroupCustomForm
# from django.urls import path
# from django.conf.urls import url

from import_export.admin import ImportExportModelAdmin, ImportExportMixin

from .models import (
    User,
    Group
)


@admin.register(User)
class UserAdmin(ImportExportMixin, UserAdmin):
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    list_display = ["email", "is_superuser", "get_groups"]

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2',)
        }),
    )

    def get_groups(self, obj):
        if obj.groups:
            groups = ""
            for g in obj.groups.values_list('name', flat=True):
                groups += str(g) + " "
            return groups

    get_groups.short_description = "Grupos"
    


@admin.register(Group)
class GroupAdmin(ImportExportModelAdmin):
    form = GroupCustomForm
    filter_horizontal = ['users', 'maintenance_plants']
