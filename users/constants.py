from django.utils.translation import ugettext_lazy as _

MAINTENANCE_USER = 'maintenance_user'
MAINTENANCE_SUPERVISOR = 'maintenance_supervisor'
QUALITY_USER = 'quality_user'
QUALITY_SUPERVISOR = 'quality_supervisor'
OPERATIONS_SUPERVISOR = 'operations_supervisor'
OPERATIONS_USER = 'operations_user'
GROUP_ROLES = [
    (MAINTENANCE_USER, 'Maintenance user'),
    (MAINTENANCE_SUPERVISOR, 'Maintenance supervisor'),
    (QUALITY_USER, 'Quality user'),
    (QUALITY_SUPERVISOR, 'Quality supervisor'),
    (OPERATIONS_USER, 'Operations user'),
    (OPERATIONS_SUPERVISOR, 'Operations supervisor'),
]
