from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from base.models import Model
from organization.models import LocalBusiness

from . import constants
from .validators import UnicodeUsernameValidator


class User(AbstractUser):
    username = models.CharField(
        _('Username'), max_length=150, unique=True,
        validators=[UnicodeUsernameValidator()],
        help_text=_('Required. 150 characters or fewer. Letters, digits and ./+/-/_ only.'),
        error_messages={
            'unique': _('A user with that username already exists.'),
        },
    )
    email = models.EmailField(_('email address'), unique=True, default=None)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    class Meta:
        # unique_together = [
        #     ('pk', 'groups_ref')
        # ]
        db_table = 'auth_user'

    def save(self, *args, **kwargs):
        self.username = self.email.replace('@', '+')
        super().save(*args, **kwargs)


class Group(Model):
    role = models.CharField(
        _('Role'),
        choices=constants.GROUP_ROLES,
        default=constants.MAINTENANCE_USER,
        max_length=128,
    )

    users = models.ManyToManyField(User, related_name='groups_ref', verbose_name=_('Users'))
    maintenance_plants = models.ManyToManyField(
        LocalBusiness, related_name='groups', verbose_name=_('Maintenance plants'))

    def __str__(self):
        if self.role.split('_')[1] == 'supervisor':
            return self.role
        else:
            name = "{} - ".format(self.role)
            for plant in self.maintenance_plants.all():
                name += "{} ".format(plant.name)
            return name

    # def clean(self):
    #     for user in self.users.all():
    #         if user.groups_ref.exclude(pk=self.pk):
    #             raise ValidationError("Un usuario ya se encuentra en otro grupo")

    class Meta:
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')
        ordering = ['-pk']
