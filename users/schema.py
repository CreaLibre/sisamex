import graphene

from .queries.users import Query as user_query

class Query(
    user_query,
    graphene.AbstractType
):
    pass