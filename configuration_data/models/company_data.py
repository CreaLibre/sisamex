from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model


class Company(Model):
    title = models.CharField(_('Title'), max_length=128)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')
        ordering = ['-pk']


class PowerPlant(Model):
    title = models.CharField(_('Title'), max_length=128)
    company = models.ForeignKey(
        'configuration_data.Company',
        on_delete=models.CASCADE,
        related_name='power_plant',
        verbose_name=_('Power plant')
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Power plant')
        verbose_name_plural = _('Power plants')
        ordering = ['-pk']


class Line(Model):
    title = models.CharField(_('Line'), max_length=128)
    power_plant = models.ForeignKey(
        'configuration_data.PowerPlant',
        on_delete=models.CASCADE,
        related_name='line',
        verbose_name=_('Line')
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Line')
        verbose_name_plural = _('Lines')
        ordering = ['-pk']


class Machine(Model):
    title = models.CharField(_('Machine'), max_length=128)
    url_mt_connect = models.CharField(_('Url Mt Connect'), max_length=128)
    scanner_id = models.CharField(_('Scanner id'), max_length=128)
    controller_mode = models.CharField(_('Controller mode'), max_length=128)
    controller_warning = models.CharField(_('Controller warning'), max_length=128)
    execution = models.CharField(_('Execution'), max_length=128)
    part_count = models.CharField(_('Part count'), max_length=128)
    path_program = models.CharField(_('Path program'), max_length=128)
    path_tool_number = models.CharField(_('Path tool number'), max_length=128)
    tool_number = models.CharField(_('Tool number'), max_length=128)
    g_code = models.CharField(_('G code'), max_length=128)
    limit_micro_paro_s = models.BooleanField(_(''), default=False)
    process_server = models.BooleanField(_('Process server'), default=False)
    program_var = models.CharField(_('Program var'), max_length=128)
    
    machine_model = models.ForeignKey(
        'configuration_data.MachineModel',
        on_delete=models.SET_NULL,
        verbose_name=_('Machine model'),
        related_name='Machine',
        null=True, blank=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Machine')
        verbose_name_plural = _('Machines')
        ordering = ['-pk']

