from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from base.models import Model


class MachineBrand(Model):
    title = models.CharField(_('Machine brand'), max_length=128)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Machine brand')
        verbose_name_plural = _('Machine brands')
        ordering = ['-pk']


class MachineModel(Model):
    title = models.CharField(_('Machine model'), max_length=128)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Machine model')
        verbose_name_plural = _('Machine models')
        ordering = ['-pk']

