from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from base.models import Model


class HistoricalStatus(Model):
    machine = models.ForeignKey(
        'configuration_data.Machine',
        on_delete=models.CASCADE,
        verbose_name=_('Machine'),
        related_name='historical_status'
    )
    date = models.DateTimeField(_('Date')),
    # paros_data3 = models.ForeignKey(
    #     'configuration_data.ParosData3',
    #     on_delete=models.CASCADE,
    #     verbose_name=_('Paros data 3'),
    #     related_name='historical_status'
    # )
    delta_time = models.FloatField(_('Delta time'))
    shift_date = models.DateTimeField(_('Shift date'))
    shift = models.IntegerField(_('Shift'))
    mode = models.CharField(_('Mode'), max_length=128)
    alarm = models.CharField(_('Alarm'), max_length=128)
    corte_viruta = models.CharField(_('Corte viruta'), max_length=128)
    pieces = models.FloatField(_('Pieces'))
    # model_piece = models.ForeignKey(
    #     'configuration_data.ModelPiece',
    #     on_delete=models.CASCADE,
    #     verbose_name=_('Model piece'),
    #     related_name='historical_status'
    # )
    # piece = models.ForeignKey(
    #     'configuration_date.Piece',
    #     on_delete=models.CASCADE,
    #     verbose_name=_('Piece'),
    #     related_name='historical_status'
    # )

    def __str__(self):
        return machine.title


    class Meta:
        verbose_name = _('Historical status')
        verbose_name_plural = _('Historical status')
        ordering = ['-pk']


class OnlineStatus(Model):
    machine = models.ForeignKey(
        'configuration_data.Machine',
        on_delete= models.CASCADE,
        verbose_name=_('Machine'),
        related_name='online_status'
    )
    variable_date = models.DateTimeField(_('Variable Date'))
    on = models.BooleanField(_('On'), default=False)
    off = models.BooleanField(_('Off'))
    alarmed = models.BooleanField(_('Alarmed'))
    stopped = models.BooleanField(_('Stopped'))
    corte_de_viruta = models.BooleanField(_('Corte de Viruta'))
    online_status = models.FloatField(_('Online Status'))


    def __str__(self):
        return machine.title

    class Meta:
        verbose_name = _('Online status')
        verbose_name_plural = _('Online status')
        ordering = ['-pk']
