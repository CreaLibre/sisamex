from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import (
    Company,
    PowerPlant,
    Line,
    Machine,
    MachineBrand,
    MachineModel,
    HistoricalStatus,
    OnlineStatus
)

@admin.register(Company)
class CompanyAdmin(ImportExportModelAdmin):
    pass

@admin.register(PowerPlant)
class PowerPlantAdmin(ImportExportModelAdmin):
    pass

@admin.register(Line)
class LineAdmin(ImportExportModelAdmin):
    pass

@admin.register(Machine)
class MachineAdmin(ImportExportModelAdmin):
    pass

@admin.register(MachineBrand)
class MachineBrandAdmin(ImportExportModelAdmin):
    pass

@admin.register(MachineModel)
class MachineModelAdmin(ImportExportModelAdmin):
    pass

@admin.register(HistoricalStatus)
class HistoricalStatusAdmin(ImportExportModelAdmin):
    pass

@admin.register(OnlineStatus)
class OnlineStatusAdmin(ImportExportModelAdmin):
    pass
