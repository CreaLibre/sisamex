from django.apps import AppConfig


class ConfigurationDataConfig(AppConfig):
    name = 'configuration_data'
