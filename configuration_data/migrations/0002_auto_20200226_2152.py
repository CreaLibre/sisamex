# Generated by Django 3.0.2 on 2020-02-26 21:52

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('configuration_data', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MachineBrand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('title', models.CharField(max_length=128, verbose_name='Machine brand')),
            ],
            options={
                'verbose_name': 'Machine brand',
                'verbose_name_plural': 'Machine brands',
                'ordering': ['-pk'],
            },
        ),
        migrations.CreateModel(
            name='MachineModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('title', models.CharField(max_length=128, verbose_name='Machine model')),
            ],
            options={
                'verbose_name': 'Machine model',
                'verbose_name_plural': 'Machine models',
                'ordering': ['-pk'],
            },
        ),
        migrations.DeleteModel(
            name='CompanyBrand',
        ),
        migrations.DeleteModel(
            name='CompanyModel',
        ),
        migrations.AddField(
            model_name='machine',
            name='limit_micro_paro_s',
            field=models.BooleanField(default=False, verbose_name=''),
        ),
        migrations.AddField(
            model_name='machine',
            name='machine_model',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='Machine', to='configuration_data.MachineModel', verbose_name='Machine model'),
        ),
    ]
