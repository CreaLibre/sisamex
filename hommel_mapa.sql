-- Table: public.hommel_mapa

-- DROP TABLE public.hommel_mapa;

CREATE TABLE public.hommel_mapa
(
    id integer NOT NULL DEFAULT nextval('hommel_mapa_id_seq'::regclass),
    uuid uuid NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    version character varying(20) COLLATE pg_catalog."default",
    creation_date timestamp with time zone NOT NULL,
    file_id integer,
    CONSTRAINT hommel_mapa_pkey PRIMARY KEY (id),
    CONSTRAINT hommel_mapa_uuid_key UNIQUE (uuid),
    CONSTRAINT hommel_mapa_file_id_fc75b66a_fk_hommel_archivo_id FOREIGN KEY (file_id)
        REFERENCES public.hommel_archivo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.hommel_mapa
    OWNER to evient;
-- Index: hommel_mapa_file_id_fc75b66a

-- DROP INDEX public.hommel_mapa_file_id_fc75b66a;

CREATE INDEX hommel_mapa_file_id_fc75b66a
    ON public.hommel_mapa USING btree
    (file_id ASC NULLS LAST)
    TABLESPACE pg_default;