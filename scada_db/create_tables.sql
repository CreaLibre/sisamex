create table sisbox_maquinas(id_variable integer, nombre_variable varchar(50), id_maquina integer, nombre_maquina varchar(50));
create table sisbox_valores(id integer, valor decimal(18,2), id_maquina integer, id_variable integer, fecha date);
create table sisbox_variables(id_variable integer, nombre_variable varchar(50));