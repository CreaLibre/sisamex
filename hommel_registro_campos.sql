-- Table: public.hommel_registro_campos

-- DROP TABLE public.hommel_registro_campos;

CREATE TABLE public.hommel_registro_campos
(
    id integer NOT NULL DEFAULT nextval('hommel_registro_campos_id_seq'::regclass),
    uuid uuid NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    value_field double precision,
    map_fields_id integer,
    registry_id integer,
    CONSTRAINT hommel_registro_campos_pkey PRIMARY KEY (id),
    CONSTRAINT hommel_registro_campos_uuid_key UNIQUE (uuid),
    CONSTRAINT hommel_registro_camp_map_fields_id_c9a5334a_fk_hommel_ma FOREIGN KEY (map_fields_id)
        REFERENCES public.hommel_mapa_campos (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED,
    CONSTRAINT hommel_registro_camp_registry_id_03d3ca8d_fk_hommel_re FOREIGN KEY (registry_id)
        REFERENCES public.hommel_registro (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        DEFERRABLE INITIALLY DEFERRED
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.hommel_registro_campos
    OWNER to evient;
-- Index: hommel_registro_campos_map_fields_id_c9a5334a

-- DROP INDEX public.hommel_registro_campos_map_fields_id_c9a5334a;

CREATE INDEX hommel_registro_campos_map_fields_id_c9a5334a
    ON public.hommel_registro_campos USING btree
    (map_fields_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: hommel_registro_campos_registry_id_03d3ca8d

-- DROP INDEX public.hommel_registro_campos_registry_id_03d3ca8d;

CREATE INDEX hommel_registro_campos_registry_id_03d3ca8d
    ON public.hommel_registro_campos USING btree
    (registry_id ASC NULLS LAST)
    TABLESPACE pg_default;